
label act3:
    
    n2 "Monsters.{p}\nThings that haunts us in our nightmares.{p}\nThe villain in every childhood stories, destined to be defeated by the mighty hero.{p}\nA moral lesson to every child, so that they can grow up to be the hero in their story – their life."
    n2 "Unfortunately for those kids, when they grow up, they'll see that there is no such thing as a hero and monster in most stories.{p}\nIt's all about perspective. One man's monster is another man's hero.{p}\nA wise man once said : 'History is written by victors.'{p}\nHe basically said, that the loser will always be the villain.{p}\nWell, they can't exactly argue from their graves, can they?"
    n2 "When David cut Goliath's head off, what did the Philistines saw?{p}\nA monster capable of taking down their strongest warrior.{p}\nYet the story told us that David the hero defeated Goliath the monster, not the other way around.{p}\nSee? History is written by the victors."
    n2 "No matter what kind of atrocities we do, we can be the hero, as long as we win.{p}\nNo matter how underhanded or cowardly our tactics are, as long as we can rewrite the story.{p}\nVictory is everything. No matter what sacrifices I have to make. No matter the cost.{p}\nAs long as I have victory, I have the power to shape the future.{p}\nAnd I also have the power to rewrite the past."

    play music "BGM/revealed.mp3" fadein 4
    show card act3_0 with Dissolve(4.0)
    centered ""
    scene bg bridge under with Dissolve(2.0)
    show card act3_1 with dissolve
    centered ""
    hide card act3_1 with dissolve
    
    
    jn "There they are. Another flock of humanity's all time low."
    extend "\nMan, Arthur's got some bad luck getting involved with this bunch. I guess I'll observe them for a while before making a move."
    
    jn "There are four of them. One is carrying a gun. Looks like the pack leader. I'll have to make him the priority. "
    
    jn "A 4 vs 1 huh... This is going to be fun."
    
    n2 "Still staying hidden, Julie reach for a bunch of coins from her pocket and dropped them to the concrete ground.{nw}"
    play sound "SFX/coinmetalhit.mp3"
    extend " It hit the ground with a rowdy metallic ringing.{w}\n\n\"Hm?\"{w}\n\nOne guy heard the sound. With his friends still busy chatting, he decided to investigate."

    n2 "Julie sees his shadow drawing closer to her.{w}\n\nCloser.{w}\n\nCloser."

    n2 "Just before the guy sees what's beyond the corner, she pull on his arm, tipping him off balance.{w}\n\n\"What the f—\"{nw}"
    play sound "SFX/punch.mp3"
    
    n2 "{w}In one swift motion, she smashed his head against the wall. Just tossed it like slamming a car door shut.{w}\n\nThe sound of his skull banging against concrete echoes through the tunnel.{w}\n\nOne down. Three left.{w}\n\nShe slips her trusty brass knuckle into her fist. As her heart beats faster and adrenaline starts kicking in, a smile forms on her face."
    
    n2 "Stepping out of her cover, she stand before three guys. Her devilish smile and bloodthirsty eyes struck her opponents.{w}\n\nThey saw their friend. Lying unconscious in a pool of blood.{w}\n\nTheir instincts, developed from years of living as an outlaw, tells them that she's a dangerous opponent.{w}\n\n\"Is it fun?\" she said.{w}\n\n\"Huh?\""
    
    n2 "\"Is it fun bullying the weak? Extorting people for your own gain? Hmm?\" she said.{w}\n\n\"What is she talking about?\"{w}\n\n\"I don't know, man. She's bad news tho'.\"{w}\n\n\"Tch, still playing dumb, huh?\" Julie said.{w}\n\n\"Oh well, let's just get this over with.\""
    
    n2 "She calmly start to move forward. Deliberately taking slow steps, carefully watching their movement as their distance grows shorter.{w}\n\nThey start shuffling around. The boss took a step back, while the two guys takes a step forward, shielding their leader like a loyal servant.{w}\n\nOne of them start moving slowly towards Julie. Taking one step at a time.{w}\n\nOne.{w}\n\nTwo."
    
    n2 "Suddenly, Julie bolts. Charging him head-on. The guy reflexively put his arms up, trying to block her attack. Yet she passes him. And the other guy too. She's running straight for the boss. The one that has a gun. The biggest threat here. And she caught him in a surprise attack.{w}\n\n\"Oh f—\"\n\nShe buried her thumbs in his eye socket, her nails digging into his eyeballs.{nw}"
    play sound "SFX/uppercut.mp3"
    extend " Then, she brought his face down to meet her knee. The hit landed square on his forehead, with enough force to fling him into the air."
    
    n2 "As he landed on his back with a definite thud, she set her sights for the next guy.{w}\n\nThe two of them just stood there. Like a deer in headlights.{w}\n\n\"Tch! Fuck this shit!\"{w}\n\n\"Hey wait fo—\""
    
    n2 "\"You're not going anywhere.\" she said to the guy left behind, her hand grabbing his collar.{w}\n\nShe shoved him forward while tripping his left foot, making him fall flat on his face.{w}\n\nShe sat on him, then secured his arms behind his back.{w}\n\n\"Answer my questions, and you get to keep breathing.\" she said.{w}\n\n\"What the hell! Let me go!\"{w}\n\n\"Why do you keep pestering Arthur?\"{w}\n\n\"What the hell are you talkin' about, you crazy fuckin bitch?!\""
    
    n2 "{i}Sigh.{/i}{w}\n\nShe grab hold of his left wrist,{nw}"
    play sound "SFX/bonebreak.mp3"
    extend " and snapped his pinky.{w}\n\n\"AAARGH FU—\"{nw}"
    play sound "SFX/bonebreak.mp3"
    extend "\n\nThen she snapped his ring finger.{w}\n\n\"AAARGHH STOP! STOP!\""
    
    play sound "SFX/bonebreak.mp3"
    n2 "Middle finger.{w}\n\n\"AAHHH I'LL TELL YOU PLE—\"{nw}"
    play sound "SFX/bonebreak.mp3"
    extend "\n\nForefinger.{w}\n\n\"AAARRRGH STOP PLEASE I'M BEGGIN—\"{nw}"
    play sound "SFX/bonebreak.mp3"
    extend "\n\nThumb.{w}\n\n\"AAAGGHHHH!! PLEASE STOP PLEASE....\""
    
    n2 "\"Next time, think very carefully before answering my question.\"{w}\n\n\"I-I'm s-sorry...\"{w}\n\n\"One more time. Why did you intimidate Arthur?\"{w}\n\n\"I-I don't know what you're talking about...\"{w}\n\n\"Arthur Woods! Don't you remember?!\"{w}\n\n\"W-what? We did nothing to Arthur Woods...{w} The boss said not to touch him.\"{w}\n\n\"What?! What do you mean?\""
    
    n2 "And so he spilled everything he knows.{w}\n\nAbout The Syndicate.{w}\n\nAbout Milton Corp.{w}\n\nAbout Reyes.{w}\n\nAbout Arthur, and his reputation.{w}\n\nThis makes Julie extremely angry, but deep inside her mind, she thought that something doesn't add up."
    
    j "So you're saying that he's your 'business partner'?"
    
    thug "Yeah... that's what the boss told us..."
    
    j "Tch, fuck!"
    
    jn "I get off his back and start thinking."
    
    jn "I've been played. He had me dancing in his palm all this time."
    
    jn "Pacing around, I try to connect the dots. Trying to figure out his intentions."
    
    jn "Arthur works for this shady corporation. They made a deal with Reyes, these guys boss. Then he used me to... beat them up."
    extend "\nWhy? It doesn't make any sense..."
    
    jn "If he wanted to kill me, he had plenty of chances to do so. I'm guessing that's not his objective."
    extend "\nIf he want to forcefully take over these guys territory, just beating up a handful of them wouldn't be enough."
    
    jn "Wait a minute, who the hell are those people beating up Arthur in the alley? There's no guarantee that it's Reyes's men. They could very well be Arthur's."

    jn "And considering how far he's willing to go to fool me, something big must be at stake here."
    
    jn "What is his endgame? His true objective? What role do I play in his plan? "
    
    jn "How much influence do I have in his plan?"
    
    jn "Should I keep playing along?"
    
    jn "Is he the good guy? Or the bad guy? Or both?"
    
    jn "So many questions, this is getting confusing."
    extend "\nI guess all I can do now is just go home for now, mulling about here won't do me any good."
    
    jn "I shoot a glance at the guy whose fingers I just broke."
    
    jn "Should I call an ambulance?"
    
    jn "Nah, that's a bad idea. They deserved it anyway, drug dealers and whatnot."
    
    n2 "And so, she left them. Either writhing in pain or unconscious.{w}\n\nThe guy was upset, to say the least. A stranger come and give his friends brain damage, and deciding that it wasn't enough, broke his fingers.{w}\n\nA man dragging a dead body suddenly appears from the shadows. His perpetually empty eyes gazing at the victims of Julie's 'community service'.{w}\n\n\"Arthur Woods? You bas—\"{nw}"
    play sound "SFX/silencergun.mp3"
    extend "{w}\n\nHe shot him right in the forehead. Instant death."
    
    n2 "He put down the dead body he's been dragging, the guy that escaped Julie's assault, near his now dead friend.{w}\n\nHe approached the boss, still unconscious, put a gun against his head,{nw}"
    play sound "SFX/silencergun.mp3"
    extend " and pulled the trigger."
    play sound "SFX/silencergun.mp3"
    queue sound "SFX/silencergun.mp3"
    extend "\n\nSame for the others. All of them gets a burning hot lead through their brains. A bang. A final goodbye to their existence.{w}\n\nWith Reyes's men done, the other actors can now enter the stage."

    n2 "Three dead bodies enter. Dalton's men killed by the puppet master himself.{w}\n\nPlanting the seed of conflict, all he have to do now is to wait for a bountiful harvest.{w}\n\n\"Alright, everything is in place. Time to get out of here,\" Arthur said to himself.{w}\n\nWith this, a war is inevitable."
        
    stop music fadeout 1
    scene bg livingroom morning with fade
    show card act3_2 with dissolve
    centered ""
    hide card act3_2 with dissolve
    play music "BGM/secret.mp3" fadein 1
    
    play sound "SFX/handphonering.mp3" loop
    an ""
    stop sound
    
    a "Hello?"
    
    d "Arthur, are you there?"
    
    a "Yeah?"
    
    d "Oh, thank god you're still alive. I need your help, kid. Come down to the office now."
    
    a "Woah, woah, what's going on?"
    
    d "Just... just come here, I'll explain later."
    
    a "Alright then... I'll be there in half an hour."
    play sound "SFX/endcall.mp3"
    extend ""
    
    a "Right, let's get goi—{nw}"
    play sound "SFX/handphonering.mp3"
    extend ""
    
    a "Hello."
    
    j "Hey Arthur! What's up? Did you see the news this morning?"
    
    an "Julie... What is she trying to pull?"
    extend "\nIs she playing dumb? She should have realized what's happening...{w} right?"
    
    a "Yeah... The gang war I caused. What of it?"
    
    j "So it really was you...{w} I knew it."
    
    a "What do you want?"
    
    j "Answers."
    
    a "Ask away."
    
    j "I'll start with the news."
    
    j "The guys you sent me to beat up last night. I didn't kill them. Yet the news this morning reported that 7 people are found dead. Three more than the guys that were there originally."
    
    a "I was there last night. I watched you beat four grown adults into a bloody mess. I'm the one that arranged the bodies in order to start a gang war."
    
    j "You killed seven people?! You fucking murderer."
    
    an "Ugh."
    extend "\nI don't have time to argue with some crazed woman over the phone."
    
    a "Listen, I need to go now. Meet me at Mobius Café later. I'll explain everything."
    
    j "Wai—{nw}"
    play sound "SFX/endcall.mp3"
    extend ""
    stop sound
    
    stop music fadeout 1
    stop sound
    scene bg miltonhq with fade
    show card act3_3 with dissolve
    centered ""
    hide card act3_3 with dissolve
    play music "BGM/warcry.mp3" fadein 1
    
    show dalton normal with easeinleft
    a "So... basically, we're in an all out war now..."
    
    show dalton speaking
    d "Yeah... It's a shitstorm. Most of our guys, apart from the ones you see here, are either injured or dead."
    d "Not to mention the cops. Oh! They're going to have a field day with this. It's going to take a lot of cash to make them look the other way."
    d "And not to mention the cost of this...{w=0.3} misunderstanding with Reyes."
    extend "\nGoddamnit! The Syndicate's going to roast me for this!"
    
    show dalton normal
    d "Honestly, I don't know what to do anymore. The situation has gone from fine to fucked up in a span of a single night. Can't you think of something, Arthur?"
    
    a "I'll go talk to him, try to convince him to stop this madness."
    
    d "Don't bother, you'll just get killed or taken hostage."
    
    a "I'm not asking you to rescue me or anything, why should you care?"
    
    d "Suit yourself then..."
    
    a "Anyway, the only advice I can give you now is to stay in the office for now, at least until the situation calms down."
    a "The guys are here protecting you and you got bulletproof windows. Should be pretty safe until I get everything sorted out, or until The Syndicate decides that you're important enough to keep alive."
    
    a "Now, if you'll excuse me, I'll be leaving now. I got plans to meet up with my friends."
    extend "\nTry not to get killed in the meantime."
    
    stop music fadeout 1
    scene bg mobius day with fade
    show card act3_4 with dissolve
    centered ""
    hide card act3_4 with dissolve
    play music "BGM/sweetrevenge.mp3" fadein 1
    
    show ciel 3 at left with easeinleft
    c "This is suicide. A complete fucking suicide, I'm telling you. She's going to kill us both."
    
    a "Relax. We are in public, none of us can make too much noise here. This is a neutral ground."
    extend "\nAnd we still have the 'insurance' if things go south."
    
    c "Yeah, your insurance. It won't prevent us from getting killed. It'll just make it less painful."
    
    c "What the hell possessed you to take advantage of an... I don't know? Unstable gangster-beating-as-a-hobby vigilante anyway? You know it won't end well..."
    extend "\nKnowing you, I thought you would have her killed by now."
    
    a "Well I'd be lying if I say I didn't think about it. But..."
    
    show ciel 4 at left
    c "You somehow sympathize with her, seeing that you two aren't that different, and it awakens your long dead sense of morality?"
    
    a "Well... you know what they say... 'The enemy of my enemy is my friend'. Surely we can engage in a 'mutually beneficial' partnership. Something that'll benefit both of us."
    
    show ciel 2 at left
    c "So... basically, you still want to take advantage of her. "
    extend "\nUgh... can't expect anything less from you..."
    
    a "Speak of the devil, there she is."
    
    show ciel 1 at left
    show julie 6 at right with easeinright
    an "As soon as we made eye contact, she instantly radiated a strong bloodlust, directed at me. We maintained eye contact as she walk to our table. She then shoot a cautious glance at Ciel before finally sitting down."
    
    show julie 5 at right
    j "..."
    
    a "..."
    
    c "..."
    
    a "Nice weather we're having, right?"
    
    j "Cut the crap. Why is she here? Is she a part of this as well?"
    
    show ciel 2 at left
    c "You can say that I'm a... collaborator. We're certainly after the same thing."
    
    c "Doesn't mean that I'm on his side or anything. Quite the opposite, in fact. I'm making sure that he doesn't do anything too... extreme."
    
    a "And there you have it..."
    
    j "Explain what happened last night. What did you made me do?"
    
    a "You saw the news this morning right? A full-scale gang war just erupted overnight."
    
    j "I know that much. I know about Reyes and Dalton too."
    extend "\nWhat I don't understand is your motivation. Your goal."
    extend "\nClimbing the corporate ladder is one thing. Sparking a huge conflict over it doesn't make sense to me."
    
    a "I'm just avoiding suspicion. Think about it from their perspective."
    extend "\nAn overly ambitious young man climbs through their ranks too fast for their liking. I'll become a threat to them."
    a "The solution is a bait-and-switch. In this case, a gang war."
    extend "\nDalton will be seen as an incompetent leader and they'll promote me as a better alternative."
    
    show julie 6 at right
    j "Basically, you're creating a circumstances of which you'll be a better option than Dalton. Makes sense."
    
    a "...{w} Wow. I didn't know you could be so reas—"
    
    show julie 2 at right
    show ciel 9 at left
    j "Is that what you thought I'd say? Do you even realize what you have done? How many people have died because of you?"
    
    a "They're all bad guys killing each other."
    
    show ciel 3 at left
    j "What about the civilians caught in the crossfire? Have you thought about them?"
    
    show julie 8 at right
    an "..."
    
    show julie 2 at right
    j "Answer me!"
    
    c "Julie, I think you're being unreasonable."
    
    j "What?!"
    
    c "This isn't the street fight or other kind of skirmishes that you're used to."
    
    c "This is a war. Of course there will be collateral damage. The scale is on an entirely different level. You can't control every single outcome."
    
    show julie 4 at right
    j "But—"
    
    a "It's inevitable. You better come to terms with it early. There will always be casualties in war. There no such thing as a victory without sacrifice."
    
    show julie 5 at right
    j "...Don't you guys have any sympathy?"
    
    c "We do. We want to minimize as many casualties as we can. That's why we need your help."
    
    j "My help?"
    
    a "If we are going to prevent further casualties, ending the war as quickly as possible is the best way to do that."
    extend "\nThere are two ways a war can end: A stalemate, or complete destruction of either party."
    
    j "I guess you're planning to do the latter. I'm not killing anybody. No way."
    
    a "We're not killing him. We're forcing him to surrender."
    extend "\nOf course, that's not going to be easy. We have to be a bit more 'persuasive' than usual."
    
    c "Still, undertaking this kind of mission is risky. The possibility of death is still very real. That's why we need trust. Teamwork is essential for us to make it out alive. We're going into the lion's den after all."
    
    a "Julie, if we're going to do this, we need to put aside our differences. For now, at least."
    extend "\nIf you really want to save people, this is your chance. A chance to make a real difference."
    
    show julie 6 at right
    j "..."
    
    show julie 5 at right
    j "Alright. I'm in, I guess..."
    
    show ciel 1 at left
    a "Alright. Before I show you the plan, I want you all to remember."
    a "At the time of execution, my orders are absolute. We can't show any weakness in there. And persuading that old bastard isn't going to be easy."
    a "I'll tell you beforehand, it won't be pretty. So keep that in mind."
    
    stop music fadeout 1
    scene bg miltonhq with fade
    show card act3_5 with dissolve
    centered ""
    hide card act3_5 with dissolve
    play music "BGM/revealed.mp3" fadein 1
    
    play sound "SFX/pourwine.mp3"
    n2 "Pouring himself another glass of wine, Dalton glances at his head bodyguard, Mark.{w}\n\nCurrently, he's staring out the window while occasionally checking his phone, probably standing by for new reports from his subordinates.{w}\n\nDalton sinks back into his chair, comfy and orange, drowning himself in alcohol, trying to calm his nerves from the constant fear of his life.{w}\n\nHis mind starts to wander, trying to retrace his steps, figuring out how on earth did he end up in this situation. Holed up in his office, guarded by armed men, with his only consolation being a bottle of wine."
    
    n2 "\"I'm not supposed to be here,\" he thought. He should've been out there, having dinner with his wife and son, spending his night like a normal family.{w}\n\n\"Fucking Reyes...\" he mumbled after finishing his sixth glass of wine.{w}\n\nHe's reaching for the bottle, trying to pour himself another glass when...{w}\n\n\"Boss.\" Mark said. \"I need to take a call, it's from my girlfriend. Do you mind?\" he said while gesturing at the door.{w}\n\n\"Yeah sure...\" Dalton said while giving a shoo hand gesture."
    
    n2 "\"Thanks, boss,\" Mark said while closing the door.{w}\n\nHe sank himself back into his chair. {w}\n\nUnbeknownst to him, something was thrown into the room as the door closes."
    
    show white:
        alpha 1.0
        linear 5.0 alpha 0.5
    play sound "SFX/gunshot.mp3"
        
    # TODO sfx flashbang here
    n2 "An extremely loud bang and a blinding flash of light suddenly fills the room.{w}\n\nDalton's senses was immediately overwhelmed, soon he lost control of his balance and fall off his chair."
    show white:
        alpha 0.5
        linear 5.0 alpha 0
    n2 "After the loud bangs ceased, several figures walk in, scanning the room for potential threats.{w}\n\n\"You know, it's much worse than the ones in video games.\"{w}\n\n\"Of course it is, ya dingus!\"{w}\n\n\"Arthur! What do you want me to do?\"{w}\n\n\"Just subdue him, and make sure he doesn't die from heart attack.\""
    hide white
    n2 "Julie dragged him out from behind his office desk.{w}\n\n\"Just set him down near the bookcase,\" Arthur said. \"Ciel, get to work.\"{w}\n\n\"Roger! Hehe!\" she said while giving an exaggerated salute to Arthur."
    play sound "SFX/keyboard.mp3"
    extend "\n\nSitting down at Dalton's throne, Ciel immediately tries to access his computer."
    
    show white zorder 2
    show bg mobius sepia zorder 1
    show ciel 1 at left zorder 1
    show julie 5 at right zorder 1
    hide white with dissolve
    c "Is that everything?"
    
    a "Yeah, should be good."
    
    c "Okay... so I need to procure: three body armor, four stun grenade, an umbrella shaped chocolate candy, four smoke grenade, a disposable car, Taser, and a handcuff.\nAnd you expect all of this stuff to be ready tomorrow?"
    
    a "Piece of cake, right?"
    
    show ciel 2 at left
    c "Who the hell do you think you're talking to?"
    
    show julie 9 at right
    j "Umm, what's an umbrella shaped chocolate candy had to do with all of this?"
    
    a "Well, you never know when it's going to rain..."
    
    scene bg miltonhq with dissolve
    
    n2 "Julie searched Dalton for weapons and sits him down on the floor.{w}\n\n\"It's password protected,\" Ciel said. \"Interrogate him.\"{w}\n\n\"Hey! Wake up, you fat piece of shit!\" Julie shouted at the still disoriented Dalton.{w}\n\n\"He can't hear you, Julie. Be patient. Give him 5 seconds,\" Arthur said."
    
    n2 "Soon, Dalton start to regain his senses. The first thing he sees with his recovering vision is Arthur and Julie handcuffing him. As the constant ringing in his ears subside, he can finally connect the dots. Arthur betrayed him.{w}\n\n\"Arthur... I knew it. It's only a matter of time, huh?\" he said with a bitter tone.{w}\n\n\"Hey! What's your computer password! Answer me!\"{w}\n\n\"Piss off, woman! I don't answer to you!\"{w}\n\nThat statement earned him a right hook from Julie, dislodging several of his teeth.{w}\n\n\"Dalton... don't make the situation any more difficult than it already is.\""
    
    n2 "“MARK!! WHERE THE FUCK ARE YOU?! ANYBODY?!” Dalton shouted, thinking that his men might save him from this predicament.{w}\n\n“Yeah... about that. You see, there's only us four in this office right now. Everybody else is on... paid leave.” Arthur said.{w}\n\n“You're fucking dead, kid. The Syndicate will crush you like a bug.”{w}\n\n“The password, please. We're on a tight schedule here,” Arthur said.{w}\n\n“Piss off, you'll just kill me after you're done. I have nothing to gain by telling you.”"
    
    n2 "Arthur approached Dalton, took out his gun, and press it against Dalton's kneecap.{w}\n\n“Think about what you have to lose.”"
    play sound "SFX/gunshot.mp3"
    n2 "Arthur pulled the trigger.{w}\n\nBlood sprayed everywhere, staining Arthur and Julie's clothes and faces.{w}\n\nWitnessing this spectacle, Julie grinds her teeth. She's furious. Not because of his brutality that made her sick to her stomach. No, it's because she realized that she had been doing the same thing all this time. She's starting to realize the irony of her actions and ideals. She's no different from Arthur. No matter how she cuts it, both of them are monsters."
    
    show white zorder 2
    show bg mobius sepia zorder 1
    show julie 5 at right zorder 1
    hide white with dissolve

    a "Julie, if we're going to do this, we need to put aside our differences. For now, at least."
    extend "\nIf you really want to save people, this is your chance. A chance to make a real difference."
    
    j "..."
    
    an "Alright, I'll play along with your game for a while. See where you'll take me."
    extend "\nBut no matter what, in the end, I'll make you pay, you... two faced monster."
    
    j "Alright. I'm in, I guess..."
    
    scene bg miltonhq with dissolve
    
    n2 "Arthur is cleaning his face with his sleeve as Dalton screams in agony, feeling the excrutiating pain of a shattered knee.{w}\n\n“Arthur. The password.” Ciel said with an indifferent tone.{w}\n\n“Right.”{w}\n\nArthur snaps his finger several times in front of Dalton's face, trying to regain his attention, while his right hand putting his gun against Dalton's other knee.{w}\n\n“...juliacalvin... all lowercase...” Dalton said with a shaky voice.{w}\n\n“Your wife and son's name? Wow, that's some secure password there...”"
    
    n2 "Successfully gaining access to Dalton's computer, Ciel tries to copy the contents of the drive to her USB stick. A warning popped up in the monitor. {w}\n\nNot again, Ciel thought.{w}\n\n“Arthur, the drive is encrypted. We need his biometric data.”{w}\n\n“Retina?”{w}\n\n“Nah, just his fingerprints. Saying biometric makes me feel like a super hacker though.”{w}\n\n“Alright. Julie, can I borrow your knife?”"

    n2 "“I'LL DO IT! FOR FUCKS SAKE I'LL DO IT!”{w}\n\n“See, if you're being this cooperative from the beginning, I wouldn't have to get so violent now do I? Julie, help me get him up.”"
    
    n2 "Arthur and Julie get him up on his foot, both of them supporting his arms.{w}\n\n“See Dalton, you can still walk, all you need is a little he—”{nw}"
    play sound "SFX/sniper.mp3"
    show white:
        alpha 1.0
        linear 10.0 alpha 0.0
    n2 "Splat.{w}\n\nDalton's brain just got splattered all over the room.{w}\n\nNo warning, no sound.{w}\n\nIt took them a full second to process what just happened.{w}\n\nAnd by the time they realized what's happening, it's already too late."
    
    c "{size=+30}SNIPER!!{/size}"
    
    an "Her shout snapped me out of it. I need to get away. Fast."
    
    an "Dalton's arm, still wrapped around my neck, is now obstructing me. His dead body starts to go limp, and consequently, making it difficult to break free from him."
    
    an "Shit, I'll just du—{nw}"
    stop music
    play sound "SFX/sniper.mp3"
    
    show red:
        alpha 0.7
    an "{size=+30}{color=#f00}!!!!!!!!!!!!!!!{/color}{/size}"
    
    show red:
        alpha 0.7
        linear 1.0 alpha 0.5
    a "AAARGH!"
    
    c "Julie! Go get Arthur! I'll cover you!!"
    
    play sound "SFX/heartbeat1.mp3" loop
    show black behind red with dissolve
    j "Come on Arthur!"
    
    an "I-I think it hit me on my hip. It feels like a bomb went off inside me."
    extend "\nMy body...{w} {color=#f00}DIED{/color}..."
    extend "\nI don't have any strength left."
    
    j "Ciel! I got him! Let's get out of here!"
    
    an "This is it, huh. This is how I {color=#f00}die{/color}."
    
    c "Wait! I'm taking the hard drives!"
    
    an "Still...{w} it's unfair."
    
    j "Forget about it! Come on!"
    
    an "I...{w} died...{w} in their arms..."
    extend "\nShe...{w} she died alone...{w} and afraid..."
    
    c "Ten seconds!!"
    
    play sound "SFX/heartbeat2.mp3"
    a "Heh...{w} whatever...{w} I...{w} I'll take...{w} a...{w} little...{w} nap..."
    
    c "Got it!! Let's go!"
    
    $ renpy.pause(delay=5, hard=True)
    stop sound fadeout 5
    scene black with Dissolve(5.0)
    
    centered ""
    
    scene bg hideout night with Dissolve(2.0)
    
    a "{cps=1}...{/cps}{w} Where am I?"
    
    show julie 4 with dissolve
    j "Arthur!"
    
    show julie 9
    j "Don't get up, just lie down."
    
    a "Where are we?"
    
    show julie 1
    j "Ciel's hideout. We'll be safe here."
    
    hide julie with dissolve
    show card act3_6 with dissolve
    centered ""
    hide card act3_6 with dissolve
    play music "BGM/warcry.mp3" fadein 1
    
    show julie 1 with dissolve
    an "Fuck. I didn't die."
    
    a "Where's Ciel?"
    
    show julie 6
    j "She's disposing the car. She'll be back any minute now."
    
    a "I see..."
    
    a "..."
    
    j "..."
    
    a "You know... I want to ask you something."
    extend "\nWhen you found out that I was manipulating you, why did you gave me a chance to explain myself?"
    extend "\nWhy not just...{w} have your way with me?"
    
    show julie 5
    j "Well, you'll take me there, right? The root of all evil."
    
    a "I guess..."
    
    j "..."
    
    show julie 6
    a "..."
    
    show julie 5
    j "Have you ever thought about giving up?"
    
    a "Hm? Well... Yeah..."
    
    j "That your actions is somehow pointless and doesn't change anything?"
    
    a "I guess..."
    
    j "So what keeps you going? What keeps you moving forward with these...{w} acts?"
    
    a "That's none of your concern, isn't it?"
    
    j "You do realize it, right? We're not so different, you and I."
    extend "\nI want to know what makes you tick. What motivates you. What led you down this path."
    
    j "Besides, I bet you already know everything there is to know about me. My past. My reason. It's like reading a book for you. "
    extend "\nNot exactly fair, isn't it?"
    
    a "The world isn't exactly fair most of the time."
    
    j "Then, tell me what you know about me."
    
    a "Domestic abuse. Mother died. Father in prison. Trained in krav maga and personal defense."
    
    j "Where did you gain all of this information?"
    
    a "Information broker, deep web, the usual stuff..."
    
    a "Oh, and don't bother looking for my info there. I paid people to keep their mouth shut."
    
    j "Che. I guess I'll have to ask Ciel then..."
    
    a "..."
    
    a "You know what, I'll just tell you. Ciel would just spill everything anyway."
    
    a "Honestly, it's not even that interesting. It's your average tragic love story."
    extend "\nMy girlfriend got killed and I want revenge. Simple as that."
    
    j "Seems like both of us lost something important, huh?"
    extend "\nIs this also your way of coping? To repent?"
    
    stop music fadeout 0.5
    show julie 3
    c "Honeeey! I'm hoomeee!"
    extend "\nHas Arthur woken up yet?"
    
    show julie 4
    j "He just woke up, come here quick."
    
    show julie 1
    j "Looks like we ran out of time. We'll continue this some other time."
    
    show julie 5 at right with ease
        
    show ciel 6 with easeinleft
    c "Arthur! How are you feeling?"
    
    a "Sunshine and rainbows."
    
    show ciel 1 at left with ease
    c "Seems like I don't have to worry after all..."
    extend "\nOh, speaking of getting shot, check this out."
    
    show julie 6 at right
    a "It's... a bullet."
    
    play music "BGM/silent.mp3" fadein 1
    show ciel 2 at left
    c "A .338 Lapua Magnum to be exact. But this one... is different. It's depleted uranium."
    
    a "An armor piercing bullet. So that's why the vest didn't stop it."
    
    show julie 3 at right
    j "Wait, isn't uranium the stuff used for nuclear reactors? He could die from radiation!"
    
    show ciel 5 at left
    c "No, he won't. He'll be fine."
    
    show julie 7 at right
    a "It's 'depleted' uranium. Says so in the name. Even if it is radioactive, there's always an off-chance that I'll mutate and gain superpowers."
    
    show julie 1 at right
    show ciel 4 at left
    c "Well? Shall we put this bullet back in your body then? See if you would actually gain superpowers."
    
    a "Is fingering my hole once not enough for you?"
    
    show ciel 6 at left
    c "Oh yeah, here’s your chocolate umbrella. A get well soon gift."
    
    a "Thanks."
    extend "\nBy the way, you already took care of the wound, right?"
    
    show ciel 1 at left
    show julie 5 at right
    c "Yep, already stitched the wound."
    extend "\nThankfully, the bullet didn’t damage any organs. It goes through your hip and bounces off your hip bone."
    extend "\nIt’ll be a good idea to rest for now."
    
    a "What a coincidence. I don’t have any plans to run a triathlon right now."
    
    j "Yeah, right..."
    
    a "By the way, is this what you mean when you said that my life is in danger?"
    
    show ciel 2 at left
    c "Yeah, that’s why I brought you here instead of your apartment."
    extend "\nAs you can probably tell, he’s no ordinary guy. He’s able to track you down and uses a notoriously difficult to obtain ammunition. A very dangerous man."
    
    a "You seem familiar with him. Someone you know?"
    
    show ciel 1 at left
    show julie 4 at right
    c "Yeah...{w} He’s...{w} an MI6 operative, and...{w} also my mentor, at least before he’s gone rogue."
    
    c "This bullet I extracted out of you is one example of his favorite ammunition, he had this one specially made by a gunsmith in German."
    
    show julie 6 at right
    show ciel 2 at left
    c "His name... is David Royce. Sounds familiar, isn’t it?"
    
    a "Royce...{w} No... It can’t be..."
    
    c "Anna Royce’s father, yes. And he’s out for blood, hunting down the members of The Syndicate, including you."
    extend "\nEspecially you. His daughter’s boyfriend that, after her death, decides to work for the people behind the trigger."
    
    a "She... she never told me anything about her family. And I’m pretty sure she’s keeping me a secret from them as well..."
    
    show ciel 1 at left
    c "Well obviously, she might not even know her father’s job."
    extend "\nAnyway, the MI6 sent me to watch over him while he’s hunting down the members of The Syndicate and after that, eliminate him. Obviously, it’s not easy, and that’s where you come in."
    
    a "A bait..."
    
    c "Wow, I didn't know you could read minds. Correct, you’re a bait. At least, that was the plan...{w} I changed my mind."
    c "It’s my fault that you’re wounded like this. I can’t guarantee your safety after all... It’s only a matter of time before Julie gets hurt as well."
    
    show ciel 3 at left
    c "So... Arthur and Julie... Right now I’m giving you a choice...{w} to leave."
    c "I’ll set up both of you with new identities, new life. A chance to start over. To become normal again. A true salvation from your broken lives."
    c "All you have to do is leave behind your 'purpose’. Blend in with the rest of society, and never go back into the darkness again."
    
    show ciel 9 at left
    a "I refuse."
    
    show julie 5 at right
    j "Me too. I refuse."
    
    show ciel 3 at left
    c "... Do you guys even understand what you’re saying? This—"
    
    a "I understand perfectly. As far as I can see, I’m still alive. Anna is still six feet under the ground. I’m not done yet."
    
    j "I promised myself in the past. I will protect the weak from those who abuse their power and authority. I will not turn a blind eye from evil so that I can save myself. Not even if it kills me."
    
    c "Are you guys serious? I suggest you think about it carefully."
    
    j "I'm sure of it."
    
    a "There’s nothing to think about."
    
    show ciel 8 at left
    c "Allllriiiight then...{w} I guess we’re in this together now?"
    
    show ciel 1 at left
    j "I guess... until The Syndicate’s gone at least. I still have to decide what to do with you two{w} - especially you, Arthur."
    
    a "You know... Julie. I want you to realize something. At this point on there’s no more turning back."

    a "To fight monsters, we have to become monsters ourselves, throwing away our humanity for the sake of victory. I didn’t do those terrible things out of enjoyment – it’s out of necessity."
    
    j "I know. I understand perfectly. Still, you have to face the consequences of your actions. Do you really expect to walk away free after everything that you’ve just done?"
    
    a "No... Not really. That’s why I said you can do whatever the hell you want with me after all of this is over."
    
    stop music fadeout 1
    an "Not that I have much to live for anyway..."
    
    scene black with dissolve    
    n2 "And so it begins.{w}\n\nTheir journey into a man-eat-man world.{w}\n\nWithout rules. Without law. Without a shred of humanity left in them.{w}\n\nWhere the only victory there is to celebrate is coming out the other side alive.{w}\n\nA start of a long, long journey to hell."
    
    show card act3_e with dissolve
    centered ""
    hide card act3_e with dissolve
    
    # TODO
    # save progress here
    call screen credits1

    jump after_credits
    
    return