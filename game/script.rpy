﻿# Declare characters used by this game.
define ctcAnim = Character(ctc=anim.Blink("UI/ctc.png", xpos=0.80, ypos=0.88), ctc_position="fixed")
define sayChar = Character(kind=ctcAnim, window_background=Frame("UI/say_box.png",0,0))
define quoteChar = Character(kind=sayChar, what_prefix='"',what_suffix='"')
define quoteCharCG = Character(kind=ctcAnim, what_prefix='"',what_suffix='"', window_background=None,outlines=[ (absolute(2), "#00000099", absolute(0), absolute(0)) ],what_outlines=[ (absolute(2), "#00000099", absolute(1), absolute(1)) ])

define a = Character('Arthur',kind=quoteChar)
define a_CG = Character('Arthur',kind=quoteCharCG)
define an_CG = Character('...',kind=quoteCharCG,what_font="UI/fonts/Montserrat-Light.otf")
define an = Character('...',kind=sayChar,what_font="UI/fonts/Montserrat-Light.otf")
define jn = Character('Julie',kind=sayChar,what_font="UI/fonts/Montserrat-Light.otf",font="UI/fonts/Montserrat-Light.otf")
define w = Character('Waiter',kind=quoteChar,color="#aaaaaa",what_color="#aaaaaa")
define l = Character('Leon',kind=quoteChar,color="#c9c679",what_color="#c9c679")
define d = Character('Dalton',kind=quoteChar,color="#ccc7ff",what_color="#ccc7ff")
define r = Character('Reyes',kind=quoteChar,color="#6fbf67",what_color="#6fbf67")
define girl = Character('Girl',kind=quoteChar,color="#e269ac",what_color="#e269ac")
define prof = Character('Professor',kind=quoteChar,color="#a9cfee",what_color="#a9cfee")
define cSecret = Character('???',kind=quoteCharCG,color="#d0acdf",what_color="#d0acdf")
define c = Character('Ciel',kind=quoteChar,color="#ef9ab9",what_color="#ef9ab9")
define vig = Character('Vigilante',kind=quoteChar,color="#f59c5f",what_color="#f59c5f")
define thug = Character('Thug',kind=quoteChar,color="#ababab",what_color="#ababab")
define thug1 = Character('Thug 1',kind=quoteChar,color="#acacac",what_color="#acacac")
define thug2 = Character('Thug 2',kind=quoteChar,color="#bababa",what_color="#bababa")
define thug3 = Character('Thug 3',kind=quoteChar,color="#cacaca",what_color="#cacaca")
define vic = Character('Victim',kind=quoteChar,color="#aaaaaa",what_color="#aaaaaa")
define gang1 = Character('Gangster 1',kind=quoteChar,color="#d8a576",what_color="#d8a576")
define gang2 = Character('Gangster 2',kind=quoteChar,color="#b79373",what_color="#b79373")
define gang3 = Character('Gangster 3',kind=quoteChar,color="#e1a56f",what_color="#e1a56f")
define j = Character('Julie',kind=quoteChar,color="#f59c5f",what_color="#f59c5f")
define inf = Character('Informant',kind=quoteChar,color="#aaaaaa",what_color="#aaaaaa")
define un = Character('???',kind=quoteChar,color="#7ea7ff",what_color="#7ea7ff")
define tm = Character('Text Message',kind=quoteChar,color="#aaaaaa",what_color="#aaaaaa")
define n2 = Character(
    what_xalign=0, #Centers text within the window
    what_textalign=0, #Centers text within the window, just in case
    window_xalign=0.5, #Centers the window horizontally
    window_yalign=0.5, #Centers the window vertically
    what_color="#ffffff",
    window_background=None,
    what_outlines=[ (absolute(2), "#00000099", absolute(0), absolute(0)) ],
    ctc=anim.Blink("UI/ctc.png", xpos=0.83, ypos=0.87),
    ctc_position="fixed"
    )

# The game starts here.
label start:
    stop music
    stop sound
    jump act1