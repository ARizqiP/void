label act1:

    scene black
    play music "BGM/nightmare.mp3"
    
    n2 "It all started with the death of a woman.{w=0.2}\n\nAnna Royce.{w=0.2}\n\nMy girlfriend."
    
    n2 "My fault.{w=0.2}\n\nI couldn't save her.{w=0.2}\n\nI can still vividly remember how she calls for my help, holding on to what’s left of her life, and how powerless I was to do anything."
    
    n2 "From that point on, time has stopped moving for me.{w=0.2}\n\nStuck in the past, unable to let go of my feelings.{w=0.2}\n\nLike a drowning man gasping for air, I struggled to fill the empty void in my chest."

    n2 "So I searched, long and hard, for something to live for. Something to strive for.{w=0.2}\n\nAnd then, I found it.{w=0.2}\n\nVengeance.{w=0.2}\n\nThe only thing she left for me in this god forsaken world."
    
    n2 "Her legacy.{w=0.2}\n\nMy purpose.{w=0.2}\n\nThe only thing that's capable of making this dead man rise and walk again once more."
    
    n2 "It becomes my reason for existing.{w=0.2}\n\nA way to cope with the pain.{w=0.2}\n\nThe only way I can keep going, without getting crushed by the guilt.{w=0.2}\n\nThe start of a long walk to hell."
    
    # need proper animation here
    show card act1_0 with dissolve
    centered ""
    hide card act1_0 with dissolve
    stop music fadeout 1.0
    show white with Dissolve(1.0)
    play sound "SFX/alarm.mp3" loop
    scene bg bedroom morning with dissolve
    
    a "Well, good morning to you, too, Mr. Alarm Clock."
    extend "\nNow, would you {w=0.4}SHUT {w=0.2}THE {w=0.2}FUCK {w=0.2}UP!?"
    
    play sound "SFX/slam.mp3"
    with vpunch
    $ renpy.vibrate(0.25)
    play music "BGM/tripping.mp3" fadein 4.0
    an "I hit the button with the force of a thousand suns."
    
    an "Clearly, this isn't a good start for my day."
    extend "\nBut, with my hangover banging on my head like a jackhammer, I feel that my actions are justified."
    
    an "*sigh*"
    
    a "Water, need liquids in my system."
    
    play sound "SFX/door.mp3"
    an "I get up and lumber towards the living room."
    
    scene bg livingroom morning
    with dissolve
    
    queue sound "SFX/pourwine.mp3"
    an "Grabbing the glass in the kitchen and filling it with water, I try to remember the cause of this headache in the first place."
    
    a "Leon. That fucker."
    extend "\nHe said it's just a couple of shots, no going wild."
    
    an "As always the case when going out with Leon, we've gone overboard."
    an "What can you expect from two college students with too much money and time to do?"
    
    an "I sit on the sofa, trying to reclaim my bodily functions from the clutches of the devil’s liquid while looking out the window, seeing the same scenery I’ve seen many times before."
    
    an "Watching that big ball of radiation we usually call “The Sun” slowly rises through the horizon of the cityscape, its light shines through my window. Momentarily blinding me with it’s brilliance."
    
    play sound "SFX/cup.mp3"
    an "Putting down the now-empty glass on the table, I look at the clock."
    an "Class starts at 10, I should have plenty of time to make breakfast and enjoy a morning coffee like a decent human being."
    
    play sound "SFX/openfridge.mp3"
    an "Opening the fridge and checking out its content, I immediately crush my hope for a nice breakfast and coffee."
    an "The only thing left in there is bottled water and a stale broccoli."
    
    a "Looks like I’m eating out again. This is the uh..."
    extend " fourth time this week."
    
    an "And all of those three times are spent eating at the same damn cafe."
    
    an "Seriously, at least I should have a little variety from time to time."

    an "Whatever the case may be, I’m not planning to repeat that same mistake."

    an "I’m not fucking eating at Mobius again."
    extend " Period."
    
    #act 1-2
    stop music fadeout 1
    scene black with dissolve
    scene bg mobius day with dissolve
    play music "SFX/cafenoise.mp3" fadein 3.0
    show card act1_1 with dissolve
    centered ""
    hide card act1_1 with dissolve
    
    w "A cup of coffee and a tuna sandwich then. Anything else, sir?"

    a "That's all for now."
    
    w "Alright, here is today’s newspaper, sir.\nPlease enjoy."
    
    a "Thank you."
    
    an "................{w=0.4}\nYeah.{w=0.2}\nHere I am, once more."
    
    an "Did they put some kind of spell on me?{w=0.4}\nIt’s like my feet just automatically takes me here every single time."
    
    an "It must be some kind of voodoo magic shit or something."

    an "Ah, who am I kidding?{w=0.4}\nI just love their tuna sandwich.{w=0.2}\nSimple as that."
    
    an "I like tuna.{w=0.2}\nIt’s full of Omega 3.{w=0.2}\nGood for your heart."
    
    play sound "SFX/opennewspaper.mp3"
    an "I open the newspaper the nice lady had given to me earlier and immediately sees a big wonderful headline about celebrities scandal."
    stop music fadeout 1.0
    an "This world really has gone to shit."
    
    play sound "SFX/opennewspaper.mp3"
    play music "BGM/haunted.mp3"
    an "As I skim through the articles, a small headline catch my attention."

    an "\"Mysterious Vigilante Roams The Street at Night !!\"{w=0.2}\nWith a blurry shot of a red sport bike speeding through the night."
    
    an "The article talks about how lately some thugs inexplicably end up in a hospital in a middle of the night, sustaining gruesome injury."
    an "Broken bones, teeth, fingers, concussion, even cracked skull."
    extend "\nHemorrhaging and permanent physical impairment is unavoidable."
    an "Seems like they won’t be hustlin’ again anytime soon."
    
    an "Victim reports indicates that the attacker is a young woman with short hair."
    extend "\nHer identity is still under investigation."

    an "Really? What’s so mysterious about it?{w=0.2}\nI mean look at it. It’s clearly a woman.{w=0.4} How many woman rides a {w=0.2}bright{w=0.2} red{w=0.2} sport bike{w=0.2} in this city?"
    
    an "Can’t they even bother to do some more investigation?"
    extend "\nI mean, it’s just a matter of narrowing down the suspects from that lead."
    an "How many people owned that specific bike in this city."
    extend "\nHow many of them practices martial arts."
    extend "\nHow many of them has questionable past and history of assault."
    
    an "But, I guess the news outlet wants to milk the shit out of that story.{w=0.2}\nIt’d be a shame if the story was uncovered too quickly."
    
    an "The smart thing to do would be to prolong investigation indefinitely."

    an "Anyway, seems like beating up random thugs in the city is her favorite pastime."
    an "It’s especially bad when it’s our guys. Sooner or later Dalton’s going to be pissed.{w=0.2} It’s just a matter of time before I’m ordered to deal with her personally."
    
    an "What is she trying to accomplish anyway?{w=0.2}\nA wild guess would be trying cleaning up the mess that the cops didn’t touch."
    
    an "Protecting people.{w=0.2}\nBecoming an ally of justice.{w=0.2}\nThe golden standard of righteousness."
    
    an "Vanquishing evil from the face of the earth.{w=0.2}\nGiving the bad guys what they deserved."
    an "Lighting the fire of justice once more, paving the way to an ideal utopia."
    
    show black:
        alpha 0.0
        linear 1.0 alpha 0.1
    an "Hah. Don’t make me laugh.{w=0.2}\nAs if that kind of method will work."
    extend "\nYou can’t fight nature."
    
    show black:
        alpha 0.1
        linear 1.0 alpha 0.2
    an "Crimes, conflict, violence, betrayal, backstabbing...{w=0.2}\nThose are just the animalistic nature of humans, stemming from our need to survive."
    
    show black:
        alpha 0.2
        linear 1.0 alpha 0.3
    an "All the time humans try to suppress their animalistic instincts by the means of law, restraining themselves with the concept of justice and compassion."
    
    show black:
        alpha 0.3
        linear 1.0 alpha 0.4
    an "But why? Why do we need to conform to these social constructs?"
    extend "\nNo matter how you look at it, humans are animals."
    
    show black:
        alpha 0.4
        linear 1.0 alpha 0.5
    an "No matter how far society has progressed, survival of the fittest never goes away."
    
    show black:
        alpha 0.5
        linear 1.0 alpha 0.6
    an "Embrace it. Embrace the beast inside you."
    extend "\nBecome a monster, one that’s even worse than your enemies."
    
    show black:
        alpha 0.6
        linear 1.0 alpha 0.7
    an "A manifestation of their worst nightmare."
    extend "\nA dark force beyond anything that they can imagine."
    
    show black:
        alpha 0.7
        linear 1.0 alpha 0.8
    an "I’ll crush them. I’ll pay them back tenfold."
    extend "\nDoesn’t matter if it’s one person or an entire country, I’ll take them on."
    
    show black:
        alpha 0.8
        linear 1.0 alpha 0.9
    an "I’ll find whoever is responsible for her death, and I’ll…"
    
    hide black
    stop music
    w "Sir, is everything alright? You seem to be in pain?"

    a "Ah!"
    extend "\nNo, I mean, yeah, everything’s fine."
    
    play sound "SFX/cupandplate.mp3"
    w "Very well, here is your tuna sandwich and coffee.{w=0.2}\nPlease enjoy your meal and please don’t hesitate to call us if there’s anything that we can help with."

    a "Ah, it’s no problem, thank you very much."
    
    an "*sigh*"
    extend "\nThis day is getting worse by the minute."

    #act 1-3
    #scene classroom
    stop sound
    scene black with dissolve
    scene bg classroom with dissolve
    show card act1_2 with dissolve
    centered ""
    play music "SFX/classroom.mp3" fadein 1.0
    hide card act1_2 with dissolve
    
    prof "Alright, that’s all for today. Remember to submit your paper by the end of this week."
    
    an "Soon after the professor left the class, everybody start to follow suit."
    extend "\nJust as I start packing up and get up from my seat, I was stopped in my tracks by a figure entering the classroom nonchalantly."
    
    show leon normal with easeinright
    l "Hey, Arthur, my brotheeeer!"
    
    an "Leon Bradley."
    extend "\nA playboy wannabe with too much time and money on his  hands."
    
    an "But, underneath the daddy’s boy exterior lies a socially awkward but gentle man."
    
    a "Ugh, Leon. What the fuck do you want?"
    
    show leon speaking
    l "Alright, Arthur, listen carefully, you remember the girl that I have my eyes on?"
    extend "\nNow she - god knows what goes on inside her mind - agreed to go on a date with me tomorrow."
    
    show leon normal
    a "Let me guess."
    extend "\nYou got scared at the last minute and decided to drag me in on your date with her as emotional support."
    extend "\nIs that right?"
    
    show leon speaking
    l "You read my mind. As expected of my best friend."

    show leon normal
    a "Not interested. Bye."
    
    show leon speaking:
        xalign 0.5
        yalign 0.0
        easein 0.3 zoom 1.3
    l "Arthur please! I’m begging you!"
    extend "\nI promise if we get married later, I will name our children after you."
    
    show leon normal:
        easein 0.3 zoom 1
    a "I don’t want my name to be stuck on something that came out of your ballsack."
    extend "\nNow grow some spine and impress her enough that you can get a second date, you third-rate playboy!"
    
    show leon speaking
    l "Hey, don’t insult these nuts. They produce the finest seeds that only the finest women can appreciate."
    
    show leon normal
    a "The problem is you spill your seeds {w=0.2}{b}alone{/b}.{w=0.2} The quality doesn’t matter if there’s no one to take it."
    
    show leon speaking
    l "Well a lonely man’s gotta do what he’s gotta do. In fact, I’d call it solo training."
    
    show leon normal
    a "Like a samurai practicing his swordsmanship, huh?"
    
    show leon speaking
    l "Precisely. I’m ingraining every move into my body. So that when the ‘sticky’ situation arrives, I don’t panic and let muscle memory take over instead."
    
    show leon normal
    a "Well, anyway, I’m sorry but I can’t go with you tonight, I have other business to attend to."
    
    show leon speaking
    l "Well then...{w=0.4} I guess I’m just touching myself tonight..."
    
    show leon normal
    a "Solo training?"
    
    show leon speaking
    l "Yeah. That’s what I meant."
    
    hide leon speaking with easeoutleft

    #act 1-4
    stop music fadeout 1
    stop sound fadeout 1
    scene black with dissolve
    scene bg miltonhq with dissolve
    play music "BGM/chillboss.mp3" fadein 1
    show card act1_3 with dissolve
    centered ""
    hide card act1_3 with dissolve
    
    play sound "SFX/door.mp3"
    a "No one's here."
    
    an "Even though that bastard told me to show up precisely at 6."
    extend "\nThat guy needs to get fucked with a chainsaw."
    
    an "Waiting for Dalton to show his face, I take a seat."
    
    an "Man, this place is seriously cozy."
    extend "\nIt’s more like a lounge rather than an office."
    an "And the seats, they’re so nice and plush."
    extend "\nFeels like being cradled by a flock of swans."
    extend "\nAnd the fragrance just lulls you to a sleepy lavender-y heaven."
    
    an "It’ll be one of the nicest place to work at, if not for the fact that this is Milton Corporations headquarters."
    
    an "Milton Corporation...{w=0.4} huh."
    extend "\nOne of The Syndicate’s shell company."
    
    an "Disguising themselves as a pharmaceutical company, Milton Corporation had successfully run a drug trafficking operation for the last 5 years or so."
    an "A commendable achievement, I have to say."
    
    an "I still remember when I joined the company two years ago as a financial and business advisor, I was tasked with supporting Robert Dalton, the CEO of Milton Corporation."
    extend "\nAnd, how for the first few years, he was impressed by my results."
    
    an "Increased profits, smoother deals with our partners, streamlined workflow."
    extend "\nI guess he was pretty happy to have me in the company."
    extend "\nHe happily gave me more tasks and responsibility."
    
    an "Then, one day, something inside him just clicked."
    extend "\nI have too much power."
    
    an "Seeing that I took care of most of his job, he felt insecure."
    extend " Scared of losing his position and the loyalty of his men, he tries to get rid of me."
    
    an "Putting pressure on me, giving me tasks that are near impossible to accomplish, verbal abuse. The usual stuff."
    
    an "He can’t kill me of course."
    extend "\nThe Syndicate would bury him alive if he does."
    extend "\nI’m their cash cow after all."
    
    an "A vain attempt of retaliation. Too little too late."
    
    an "With the status quo broken, this company has been stuck in a power struggle between me and Dalton."
    
    an "It’s only a matter of time before—"
    play sound "SFX/door.mp3"
    d "Arthur."

    show dalton normal with easeinright
    an "Speak of the devil, there he is."
    
    a "Hey there. Any updates on the state of affairs?"
    
    show dalton speaking
    d "Don’t address me so casually."
    
    show dalton normal
    a "Of course, sir. I apologize."

    show dalton grin
    d "Good. Now, I do have a job for you."
    extend "\nRemember that Latino drug ring still crawling around in this city of ours?"
    
    a "The one led by Reyes if I recall correctly."
    extend "\nLast time I checked, they’re still a persistent bunch that values loyalty and family above profits."
    a "You sure you want to strike a deal with those old-fashioned bunch?"
    
    d "No, I’m not going to."
    extend " You will."
    
    an "I have a bad feeling about this."
    
    d "Have them give up their territory to us."
    extend "\nI have taken the liberty of arranging a meeting between you two."
    extend "\nGood luck, and don’t fuck this up."
    
    an "You didn’t even ask for my permission."
    extend "\nThis guy, I swear to god…."
    an "*sigh*"
    
    show dalton normal
    a "Alright then, expect good news tomorrow."
    play sound "SFX/pourwine.mp3"
    extend "\nHave a bottle of wine ready for the inevitable celebration, will you?"
    
    d "Don’t get cocky, kid. The guy’s tougher to crack than you think."
    
    a "Yeah… we’ll see about that..."

    #act 1-5
    stop music fadeout 1
    stop sound
    scene black with dissolve
    scene bg shipyard
    with dissolve
    play music "BGM/sneaky.mp3"
    show card act1_4 with dissolve
    centered ""
    hide card act1_4 with dissolve
    
    an "The first thing that I noticed as I enter this empty warehouse is how dusty it smells."
    extend "\nLooks like no one’s been here for a long time."
    
    an "Seems like Reyes isn't here yet."
    extend "\n*sigh*"
    extend "\nWhy is it that every single one of my business partners seems to have some kind of problem with being punctual?"
    
    an "Are they somehow allergic to showing up on time or something?"
    extend "\nIt can't be that hard to just wake up few hours earlier than usual, right?"
    
    an "Sure, you're a criminal."
    extend " You don't follow the rules, and you do things as you see fit."
    extend "\nBut please - for the love of god - have a shred of decency for the poor guy standing up at your appointment."
    
    an "I swear to god, if I had the chance, I'll fuck him with a chainsa—"
    
    r "Hey, you over there."
    extend "\nDalton’s boy, what’s his name,{w=0.2} uh,{w=0.2} Arthur!"
    
    an "Finally! I was about to die from old age here."
    
    a "Reyes, I presume.\nMy name is Arthur Woods, Dalton’s obedient lap dog and his representative today.\nPleasure to meet you."

    r "Cut the crap. I don't have all day.\nWhat do you want?"
    
    a "Well… You see… Milton Corporation would like to buy all of your asset in the vicinity of this city."
    extend"\nThis includes your territory, warehouses, and factories that you may have."

    r "I refuse. Are you fucking kidding me? What’s in it for me besides the money?"
    extend "\nWhat about my family? The people who would stick with me through thick and thin? Huh?"
    
    r "I’m not an animal, unlike you people..."
    r "Loyalty, kid. It’s hard to find these days. And something that you people clearly lack."
    
    a "Well as far as I’m concerned, there are two ways this situation can go."
    
    a "Option number 1:\nWe can use force, we can send our people to your factories and burn it to the ground, we kill all your men."
    extend "\nYou and your… ‘family’ end up dead."

    a "Police will definitely get involved, which means that we’ll have to bribe them again. After that, we have to set up our own infrastructure, and find our own men."
    extend "\nNot very cost effective if you ask me…"
    
    r "You basta—"
    
    a "Option number 2:\nWe can just get along, we use your asset, we employ your men, and you can go into early retirement with so much money you’d have no idea how to spend it."    
    
    a "We don’t have to deal with police, we don’t have to build our own infrastructure, and your men – sorry, family - becomes part of The Syndicate, effectively ensuring their safety."
    
    a "If I were you, the choice would have been obvious, Mr. Reyes."
    
    r "Well I can play your game, too."
    extend "\nGive me one good reason that I shouldn’t kill you right this moment."
    
    r "I have men in every nook and cranny of this city. I own this city."
    
    r "And my men, they are my brothers. Do you think I would betray them like a fucking animal?"
    extend "\nThey are my family, they got my back, I got theirs."
    
    r "Our relationship extends beyond 'just business' unlike you people."
    
    r "If I were you, Arthur, I would just leave quietly, and apologize for wasting my time."
    
    an "This is getting nowhere."
    extend "\nReyes isn’t cooperating."
    extend "\nLet’s try pushing from the other side."
    
    a "*sigh*"
    extend "\nTell me something, Reyes. Do you hate Dalton?"
    
    r "Are you blind?! Of course I fucking hate him! I want to rip his throat out!"
    extend "\nShowing up suddenly, going around bossing people around like he owns the place."
    r "That bastard needs someone to teach him a lesson."
    
    a "Good. We’re on the same page then."
    
    r "…{w}\nWhat?"
    
    a "Alright, from this point on, I’m speaking not as his representative, but as Arthur Woods."
    extend "\nI want to make a deal, Reyes. This way, both of us can get something out of it."
    
    r "…{w}\nI’m listening."
    
    a "For the last few months, I’ve been planning to kill him and take over his empire. A little coup d'état, if you will."

    a "I’ll tell that baldy that you need a six month transition period to sort out your crews and logistics, then you’ll hand over the assets to us. All you have to do is to set the story straight."
    
    a "Sounds good, right?"
    
    r "Why the hell should I get dragged into your little game?\nYour problems certainly has nothing to do with me or my crew."
    
    a "Oh, it has everything to do with you."
    extend "\nIf he really wanted to, he could just march into your territory and slaughter you and your crew."
    
    a "Sure, you can fight back, but how long can you resist against the full onslaught of The Syndicate?"
    
    a "The only reason we’re both standing here, uselessly arguing with each other, is because he wanted to get rid of me."
    extend "\nBecause I’m threatening his position."
    
    a "He sent me here fully prepared for me to fail, to be killed by you."
    extend "\nHe has nothing to lose."
    
    r "I thought you’re a key player there… A ‘our only chance’ type of player."
    extend "\nWouldn’t losing you be detrimental to him?"
    
    a "Whether I live or die doesn’t matter."
    extend "\nHe’s going to win either way. You’re going to lose either way. It’s just a matter of time."
    a "I’m offering you a chance, a hope of survival. But I need you to trust me."
    
    r "Trusting someone that’s planning to betray his boss?\nGive me a good reason why I should."
    
    a "This is the only way we can beat him, the only way for us to get out of this situation alive."
    extend "\nAvoiding direct confrontation is the key to victory here."
    a "Honestly, all you have to do is just lie to him. How fucking hard is that huh?!"
    extend "\nI’m the one in the lion’s den! One wrong move and I’ll face something worse than death!"
    
    a "Let me put it this way."
    extend "\nI can always kill him any other day. I have all the time in the world."
    a "You, on the other hand, are running out of it.\nI’m your only chance of survival."
    extend "\nDo you understand your position now?!"
    
    r "Alright! Fine! How the hell did I get dragged into this fucking mess anyway?!"
    
    r "You."
    
    r "You better fucking promise me that you’re going to fix this shit, or you’re fucking dead, kiddo."
    
    a "So... we have a deal?"
    
    r "Deal. You better not fucking messing with me kid, or you’re in a world of a fucking pain."

    r "Goddamned Syndicate.\nI got fucking dragged into this fucking mess over the fucking night. That fucking piece of shit better be dead soon."
    
    a "I’ll make sure of it. Don’t worry."
    
    #act 1-5b
    stop music fadeout 1
    scene black with dissolve
    scene bg livingroom night with dissolve
    show card act1_5 with dissolve
    centered ""
    hide card act1_5 with dissolve

    play sound "SFX/handphonering.mp3"
    an ""
    
    d "I just got off the phone with Reyes. He says he needs a six month transition period. Is that the deal?"
    
    a "Yeah, pretty reasonable, don’t you think?"
    
    d "I suppose. Are you sure he won't pull anything funny?"
    
    a "I don’t think he’s capable of such feat."
    
    d "Very well, I’ll take it from here."
    
    play sound "SFX/endcall.mp3"

    an "He ended the call."
    extend "\nHe wanted to claim the achievement for himself, huh?"
    extend "\nNot that it matters."
    
    an "Okay, it’s still too early to go to sleep."
    extend "\nHow should I spend the rest of the evening?"
    extend "\nHanging out with Leon would be pretty nice, I guess."

    play sound "SFX/dialphone.mp3"
    a ""
    extend "Good evening, you magnificent bastard."
    extend "\nLet's go and get something to eat."
    
    #act 1-6
    scene black with dissolve
    scene bg vestra night with dissolve
    show card act1_6 with dissolve
    centered ""
    play music "SFX/crowd.mp3" fadein 1.0
    hide card act1_6 with dissolve

    l "This gentleman would accompany you to the ends of the world. Your wish is my command, milady."
    
    girl "Umm no, we’re good. Thanks for the offer."
    
    an "What the hell is that idiot doing?!"
    
    l "A ring then?"
    extend "\nI’ll buy you a ring! One with a huge diamond on top!"
    extend "\nMoney is not an issue for this gentleman!"
    
    girl "No, umm, we need to go, I’m sorry. Come on, let’s get out of here."
    
    girl "What a creepy guy!"
    
    an "The girls left, leaving that hollow shell used to be called Leon standing alone in this crowded boulevard, contemplating his sad existence."
    
    an "I should probably console him. That’s what friends are for, right?"
    
    show leon normal with easeinright
    a "What a shame. Just a little bit more and you would’ve gotten a restraining order."
    
    show leon speaking
    l "Ugh, don’t you have any sympathy?"
    extend "\nMy fragile little heart can only take so much abuse before shattering completely and leaving me a soulless husk."
    
    l "Is that what you wish for? You monster."
    
    show leon normal
    a "My friend, let me tell you something."
    extend "\nMy sympathy is reserved for humans and animals - intelligent living beings, if you will."
    a "You, on the other hand, are quite difficult to sympathize with."
    extend "\nYou see, your intelligence is about on par with an amoeba"
    extend " – sorry amoeba, didn’t mean to offend you."
    a "As such, you should lack the capacity to feel and think, even to feel pain."
    a "And you expect to swoon girls with your charm?"
    extend "\nYou’re dreaming, man."
    extend "\nYou should just give up and accept the fact that you’ll never touch a pussy for the rest of your lives."
    
    show leon speaking
    l "I apologize for letting my gentlemanly charm run amok."
    extend "\nSurely drowning in pussy isn’t really high on your list of priority, is it, Arthur?"
    
    l "Poor soul, oozing with jealousy yet wouldn’t admit to my charm and good looks are superior to you in every way imaginable."
    
    l "Just admit it. Admit that you have been defeated."
    extend "\nLet your envy burst forth and I’ll bask in it, revel in it."
    
    l "Now go ahead, let it flow."
    
    show leon normal
    a "If looking like a retarded self-loving narcissist idiot is my sole purpose in life, then maybe yes, I am envious of you."
    
    show leon speaking
    l "Aha! See?! You are jealous!"
    
    show leon normal
    an "*sigh*"
    a "I can’t believe that you’re the sperm that won."
    
    show leon speaking
    l "God works in mysterious ways, huh?"
    
    show leon normal
    a "Yes he does, Leon. Yes he does."
    
    show leon speaking
    l "Anyway, why didn’t you come to class this morning?"
    l "If you tell me that you’re dating a girl while skipping class, I’ll fucking stab you."
    extend "\nBros before hoes, remember?"
    extend "\nYou aren’t allowed to nab a girl before I do."
    
    show leon normal
    a "Would you believe me if I say that I masturbated for six hours non-stop while pretending to be a cactus?"
    extend "\nIt’s not like I’m a boyfriend material for most girls anyway."
    
    a "But honestly, man, you really need to work on your social skills."
    a "Who the fuck teaches you to impress girls anyway?"
    extend "\nWhoever it is needs to get fucked with a chainsaw."
    
    show leon speaking
    l "Why do you love that expression so much?"
    extend "\n'Getting fucked with a chainsaw' doesn’t sound very appealing to me."
    l "It does roll off the tongue nicely though."
    
    show leon normal
    a "Stop dodging my question!"
    
    show leon speaking
    l "Forget about it."
    extend "\nLet’s stop standing around like idiots and actually get something to eat. I’m starving already."
    extend "\nLet’s just have that ramen over there."
    
    hide leon with easeoutleft
    an "And he just walks away."
    
    an "This guy."
    extend "\nHe’s pretty sharp sometimes, despite what his appearance and mannerisms may tell you."
    
    an "He always skirt around the topics of our personal lives."
    extend "\nCarefully choosing his words to not let anything slip out."
    extend "\nA mystery, that guy."
    
    an "He’s been a good friend I suppose. Wouldn’t wish for anyone else."
    extend "\nHe genuinely acknowledges me as a friend, even though I never told him anything about me."
    
    an "It’s...{w=0.5} strange."
    extend "\nWe have been friends for at least a year and a half now."
    extend "\nYet I didn’t know much about him."
    an "Despite our buddy routine, we keep our lives pretty much separate."
    
    an "I certainly do enjoy his company, no doubt."
    extend "\nAn endless source of entertainment, that guy."
    an "Despite that, we’re not close."
    extend "\nWe’re buddies, but we’re not friends."
    extend "\nThis strange relationship works for us."
    
    an "We keep our stories to ourselves. And for good reasons."
    
    show leon speaking at left with easeinleft
    l "Hey! Are you coming or what?!"
    
    a "Yeah!"
    hide leon speaking at left with easeoutleft

    #act 1-7
    stop music fadeout 1
    scene black with dissolve
    play music "SFX/rain.mp3" fadein 1
    scene bg livingroom night with dissolve
    show card act1_7 with dissolve
    centered ""
    hide card act1_7 with dissolve
    
    play sound "SFX/door.mp3"
    stop music fadeout 3.0
    an "Made it."
    extend "\nThat thunderstorm came out of nowhere."

    scene bg bedroom night with dissolve
    
    an "Grabbing a towel in my room, I see a notification for unread email on my work account."
    
    an "Work is never over huh? I feel like a mule lately, with Dalton’s requests and all."
    an "I decide to check it out, maybe Reyes is giving Dalton a hard time or something."
    a "Okay. Let's see."
    
    play sound "SFX/mouseclick.mp3"
    play music "BGM/secret.mp3"
    an "Wait a second. This is weird."
    extend "\nthrowaway1988765@mail.com. A throwaway email account."
    
    an "I never shared this address with anyone. It’s a strictly work address."
    
    an "The subject simply says :\n\"I see you.\""
    extend "\nI have a bad feeling about this."

    an "But as always, curiosity gets the better of me. So I decide to open it."
    play sound "SFX/mouseclick.mp3"
    extend "\nAnd that is probably one of the worst mistakes I’ve ever made."
    
    n2 "I see you, Arthur Woods.\n\nI know what you are up to. Your motivation. Your raison d'etre. Anna Royce. The Syndicate. Milton Corporation. Robert Dalton. Even your shady deal with Reyes this morning. I know them all.\n\nI saw everything, Arthur. You can’t hide from me. I know where you are.\n\nMeet me at the docks tomorrow at 10 pm.\n\nIf you didn’t show up, you can guess what would happen.\n\nGood night."

    stop music fadeout 2
    scene black with dissolve
    show card act1_e with dissolve
    centered ""
    hide card act1_e with dissolve
    show white with dissolve

    # TODO
    # save progress here

    jump act2
    return