﻿# This file is in the public domain. Feel free to modify it as a basis
# for your own screens.

# Note that many of these screens may be given additional arguments in the
# future. The use of **kwargs in the parameter list ensures your code will
# work in the future.

##############################################################################
# Say
#
# Screen that's used to display adv-mode dialogue.
# http://www.renpy.org/doc/html/screen_special.html#say
screen say(who, what, side_image=None, two_window=False):

    # Decide if we want to use the one-window or two-window variant.
    if not two_window:

        # The one window variant.
        window:
            id "window"

            has vbox:
                style "say_vbox"

            if who:
                text who id "who"

            text what id "what"

    else:

        # The two window variant.
        vbox:
            style "say_two_window_vbox"

            if who:
                window:
                    style "say_who_window"

                    text who:
                        id "who"

            window:
                id "window"

                has vbox:
                    style "say_vbox"

                text what id "what"

    # If there's a side image, display it above the text.
    if side_image:
        add side_image
    else:
        add SideImage() xalign 0.0 yalign 1.0

    # Use the quick menu.
    use quick_menu


##############################################################################
# Choice
#
# Screen that's used to display in-game menus.
# http://www.renpy.org/doc/html/screen_special.html#choice

screen choice(items):

    window:
        style "menu_window"
        xalign 0.5
        yalign 0.5

        vbox:
            style "menu"
            spacing 2

            for caption, action, chosen in items:

                if action:

                    button:
                        action action
                        style "menu_choice_button"

                        text caption style "menu_choice"

                else:
                    text caption style "menu_caption"

init -2:
    $ config.narrator_menu = True

    style menu_window is default

    style menu_choice is button_text:
        clear

    style menu_choice_button is button:
        xminimum int(config.screen_width * 0.75)
        xmaximum int(config.screen_width * 0.75)


##############################################################################
# Input
#
# Screen that's used to display renpy.input()
# http://www.renpy.org/doc/html/screen_special.html#input

screen input(prompt):

    window style "input_window":
        has vbox

        text prompt style "input_prompt"
        input id "input" style "input_text"

    use quick_menu

##############################################################################
# Nvl
#
# Screen used for nvl-mode dialogue and menus.
# http://www.renpy.org/doc/html/screen_special.html#nvl

screen nvl(dialogue, items=None):

    window:
        style "nvl_window"

        has vbox:
            style "nvl_vbox"

        # Display dialogue.
        for who, what, who_id, what_id, window_id in dialogue:
            window:
                id window_id

                has hbox:
                    spacing 10

                if who is not None:
                    text who id who_id

                text what id what_id

        # Display a menu, if given.
        if items:

            vbox:
                id "menu"

                for caption, action, chosen in items:

                    if action:

                        button:
                            style "nvl_menu_choice_button"
                            action action

                            text caption style "nvl_menu_choice"

                    else:

                        text caption style "nvl_dialogue"

    add SideImage() xalign 0.0 yalign 1.0

    use quick_menu

##############################################################################
# Main Menu
#
# Screen that's used to display the main menu, when Ren'Py first starts
# http://www.renpy.org/doc/html/screen_special.html#main-menu

screen main_menu():

    # This ensures that any other menu screen is replaced.
    tag menu

    # The background of the main menu.
    window:
        style "mm_root"

    # The main menu buttons.
    frame:
        style_group "mm"
        xalign .98
        yalign .02
        
        has vbox

        textbutton _("Start") action Start() text_style "void_mm"
        textbutton _("Load") action ShowMenu("load") text_style "void_mm"
        textbutton _("Settings") action ShowMenu("settings") text_style "void_mm"
        textbutton _("Feedback") action OpenURL("http://void.vifthfloor.moe/feedback") text_style "void_mm"
        textbutton _("Credits") action ShowMenu("credits1") text_style "void_mm"
        textbutton _("Quit") action Quit(confirm=False) text_style "void_mm"

init -2:

    # Make all the main menu buttons be the same size.
    style mm_button:
        size_group "mm"
    style void_mm:
        size 36
        xalign .5
        yalign .5



##############################################################################
# Navigation
#
# Screen that's included in other screens to display the game menu
# navigation and background.
# http://www.renpy.org/doc/html/screen_special.html#navigation
screen navigation():

    # The background of the game menu.
    window:
        style "gm_root"

    # The various buttons.
    frame:
        style_group "gm_nav"
        xalign .98
        yalign .98

        has hbox

        textbutton _("Back") action Return()
        textbutton _("Settings") action ShowMenu("settings")
        textbutton _("Save Game") action ShowMenu("save")
        textbutton _("Load Game") action ShowMenu("load")
        textbutton _("Main Menu") action MainMenu()
        textbutton _("Quit") action Quit()

init -2:

    # Make all game menu navigation buttons the same size.
    style gm_nav_button:
        size_group "gm_nav"


##############################################################################
# Save, Load
#
# Screens that allow the user to save and load the game.
# http://www.renpy.org/doc/html/screen_special.html#save
# http://www.renpy.org/doc/html/screen_special.html#load

# Since saving and loading are so similar, we combine them into
# a single screen, file_picker. We then use the file_picker screen
# from simple load and save screens.

screen file_picker():

    frame:
        style "file_picker_frame"

        has vbox
        vbox:
            imagemap:
                ground "UI/settings save load.png"

        $ columns = 2
        $ rows = 5

        # Display a grid of file slots.
        vbox:
            grid columns rows:
                transpose True
                xfill True
                style_group "file_picker"

                # Display ten file slots, numbered 1 - 10.
                for i in range(1, columns * rows + 1):

                    # Each file slot is a button.
                    button:
                        action FileAction(i)
                        xfill True

                        has hbox

                        # Add the screenshot.
                        add FileScreenshot(i)

                        $ file_name = FileSlotName(i, columns * rows)
                        $ file_time = FileTime(i, empty=_(""))
                        $ save_name = FileSaveName(i)

                        text "[file_name]. [file_time!t]\n[save_name!t]" size 30

                        key "save_delete" action FileDelete(i)


screen save():

    # This ensures that any other menu screen is replaced.
    tag menu

    use navigation
    frame:
        style "file_picker_frame"

        has vbox
        vbox:
            imagemap:
                ground "UI/save.png"

        $ columns = 2
        $ rows = 5

        # Display a grid of file slots.
        vbox:
            grid columns rows:
                transpose True
                xfill True
                style_group "file_picker"

                # Display ten file slots, numbered 1 - 10.
                for i in range(1, columns * rows + 1):

                    # Each file slot is a button.
                    button:
                        action FileAction(i)
                        xpadding 10
                        ypadding 10
                        xmargin 10
                        ymargin 5

                        hbox:
                            xfill True
                            # Add the screenshot.
                            add FileScreenshot(i)

                            $ file_name = FileSlotName(i, columns * rows)
                            $ file_time = FileTime(i, empty=_(""))

                            text "[file_name]. [file_time!t]" size 30 yalign 0.5 pos(-30,0)
                            if file_time=="":
                                text "" size 20 color "#aaaaaa" yalign 0.5 pos(90,0)
                            else:
                                text "Overwrite" size 20 color "#aaaaaa" yalign 0.5 pos(90,0)
                            key "save_delete" action FileDelete(i)

screen load():

    # This ensures that any other menu screen is replaced.
    tag menu

    use navigation
    frame:
        style "file_picker_frame"

        has vbox
        vbox:
            imagemap:
                ground "UI/load.png"

        $ columns = 2
        $ rows = 5

        # Display a grid of file slots.
        vbox:
            grid columns rows:
                transpose True
                xfill True
                style_group "file_picker"

                # Display ten file slots, numbered 1 - 10.
                for i in range(1, columns * rows + 1):

                    # Each file slot is a button.
                    button:
                        action FileAction(i)
                        xpadding 10
                        ypadding 10
                        xmargin 10
                        ymargin 5

                        hbox:
                            xfill True
                            # Add the screenshot.
                            add FileScreenshot(i)

                            $ file_name = FileSlotName(i, columns * rows)
                            $ file_time = FileTime(i, empty=_(""))

                            text "[file_name]. [file_time!t]" size 30 yalign 0.5 xpos -90

                            key "save_delete" action FileDelete(i)

init -2:
    style file_picker_frame is menu_frame
    style file_picker_nav_button is small_button
    style file_picker_nav_button_text is small_button_text
    style file_picker_button is large_button
    style file_picker_text is large_button_text

init python:    
    style.button['saveload'].background = "UI/settings save load.png"
    style.button['saveload'].xalign = 0.5
    style.file_picker_button.background = "#333333"
    style.file_picker_frame.xpadding = 0

##############################################################################
# settings
#
# Screen that allows the user to change the settings.
# http://www.renpy.org/doc/html/screen_special.html#prefereces

screen settings():

    tag menu

    # Include the navigation.
    use navigation
      
    frame xpadding 0:
        has vbox
        vbox:
            imagemap:
                ground "UI/settings.png"

        # Put the navigation columns in a three-wide grid.
        grid 3 1:
            style_group "prefs"
            xfill True

            # The left column.
            vbox:
                frame:
                    style_group "pref"
                    has vbox

                    label _("Display")
                    textbutton _("Window") action Preference("display", "window")
                    textbutton _("Fullscreen") action Preference("display", "fullscreen")

                frame:
                    style_group "pref"
                    has vbox

                    label _("Transitions")
                    textbutton _("All") action Preference("transitions", "all")
                    textbutton _("None") action Preference("transitions", "none")

                frame:
                    style_group "pref"
                    has vbox

                    textbutton _("Joystick...") action Preference("joystick")


            vbox:
                frame:
                    style_group "pref"
                    has vbox

                    label _("Skip")
                    textbutton _("Seen Messages") action Preference("skip", "seen")
                    textbutton _("All Messages") action Preference("skip", "all")

                frame:
                    style_group "pref"
                    has vbox

                    textbutton _("Begin Skipping") action Skip()

            vbox:
                frame:
                    style_group "pref"
                    has vbox

                    label _("Music Volume")
                    bar value Preference("music volume")

                frame:
                    style_group "pref"
                    has vbox

                    label _("Sound Volume")
                    bar value Preference("sound volume")

                    if config.sample_sound:
                        textbutton _("Test"):
                            action Play("sound", config.sample_sound)
                            style "soundtest_button"

                frame:
                    style_group "pref"
                    has vbox

                    label _("Text Speed")
                    bar value Preference("text speed")

init -2:
    style pref_frame:
        xfill True
        xmargin 5
        top_margin 5

    style pref_vbox:
        xfill True

    style pref_button:
        size_group "pref"
        xalign 1.0

    style pref_slider:
        xmaximum 192
        xalign 1.0

    style soundtest_button:
        xalign 1.0


##############################################################################
# Yes/No Prompt
#
# Screen that asks the user a yes or no question.
# http://www.renpy.org/doc/html/screen_special.html#yesno-prompt

screen yesno_prompt(message, yes_action, no_action):

    modal True

    window:
        style "gm_root"

    frame:
        style_group "yesno"

        xfill True
        xmargin .05
        ypos .1
        yanchor 0
        ypadding .05

        has vbox:
            xalign .5
            yalign .5
            spacing 30

        label _(message):
            xalign 0.5

        hbox:
            xalign 0.5
            spacing 100

            textbutton _("Yes") action yes_action
            textbutton _("No") action no_action

    # Right-click and escape answer "no".
    key "game_menu" action no_action

init -2:
    style yesno_button:
        size_group "yesno"

    style yesno_label_text:
        text_align 0.5
        layout "subtitle"


##############################################################################
# Quick Menu
#
# A screen that's included by the default say screen, and adds quick access to
# several useful functions.
screen quick_menu():

    # Add an in-game quick menu.
    frame:
        style_group "quick"

        xalign 1.0
        yalign 1.0
        xminimum 162
        xmaximum 162
        yminimum 235
        ymaximum 235
                
        vbox:
            xalign 0.5
            yalign 0.5
            imagebutton auto "UI/prev_%s.png" action Rollback() pos (0,-19)
            imagebutton auto "UI/menu_%s.png" action ShowMenu("settings") pos (0,-12)
            imagebutton auto "UI/auto_%s.png" action Preference("auto-forward", "toggle") pos (0,-4)
            imagebutton auto "UI/skip_%s.png" action Skip() pos (0,3)

init -2:
    style quick_button:
        is default
        background None
        xpadding 5

    style quick_button_text:
        is default
        size 20
        idle_color "#8888"
        hover_color "#ccc"
        selected_idle_color "#cc08"
        selected_hover_color "#cc0"
        insensitive_color "#4448"

label splashscreen:
    scene black
    # play sound "BGM/thegamemaker.mp3"
    $ renpy.pause(delay=0.2, hard=True)

    show splash1 with dissolve
    $ renpy.pause(delay=0.5, hard=True)
    hide splash1 with dissolve

    call screen thankyou

    show splash with dissolve
    $ renpy.pause(delay=1.5, hard=True)

    scene main_menu with dissolve
    return