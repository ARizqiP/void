screen credits1:
    
    tag menu
    window:
        background "#1d1d1e"
    frame:
        background "UI/credits1.png"
        foreground "UI/Main-menu-FRONT.png"
        vbox:
            xysize (1280,720)
            xpos .98
            ypos .98
    use nav_credits
                
screen thankyou:
    tag menu
    frame:
        xalign 0.5
        yalign 0.5
        xysize (1280,720)
        background "#1d1d1e"
        foreground "UI/Main-menu-FRONT.png"
        yfill True
        vbox:
            align (0.5, 0.5)
            text "Thank you for supporting VOID VN project!" align (0.5, 0.5)
            text "" size 20
            text "This visual novel is the one shot version of VOID," align (0.5, 0.5) size 24
            text "" size 5
            text "where parts of the story might be altered in the full version released in 2016." align (0.5, 0.5) size 24
            text "" size 10
            text "You get the chance to give your feedback on" align (0.5, 0.5) size 24
            text "" size 5
            text "technical issues, storytelling, and the story itself by filling the feedback form on the main menu." align (0.5, 0.5) size 24
            text "" size 20
            text "This version of visual novel is exclusively given to you" align (0.5, 0.5) size 24
            text "" size 5
            text "for your support on Indiegogo." align (0.5, 0.5) size 24
            text "" size 10
            text "This visual novel is not allowed to be redistributed or sold online or offline." align (0.5, 0.5) size 24
            text "" size 20
    use nav_credits
        
screen nav_credits():

    # The various buttons.
    frame:
        style_group "gm_nav"
        xalign .98
        yalign .98

        has hbox

        textbutton _(">") action Return() text_outlines [ (absolute(1), "#ffffff99", absolute(0), absolute(0)) ]