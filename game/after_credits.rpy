label after_credits:
    
    scene black with dissolve
    play music "BGM/trailer.mp3" fadein 1
    un "Report."
    
    c "I have successfully gained Arthur Woods' trust. Also, advise on the recovered hard drive, what should I do with it?"
    
    un "Just throw it away."
    
    c "It could have valuable intel. Are you sure?"
    
    un "Our mission is to eliminate David. The Syndicate is not our concern."
    
    c "Alright…"
    extend "\nBy the way, nice shot back there. You just saved me the trouble of mending broken ribs."
    
    un "Well… what do you expect from the legend himself? The bullet punched clean through him, right?"
    
    c "Yep. The transmitter easily fit in. As long as he didn’t open up his stitches and dig around with his fingers, I think we’re good."
    
    stop music fadeout 1
    un "Alright… Continue with your mission. Report back when there’s any changes. Out."
    
    return