label act2:
    
    scene black with Dissolve(5.0)
    
    play music "BGM/teaswag.mp3" fadein 3.0
    n2 "{i}Homo sapiens{/i}.{w}\n\nThe deadliest predator to ever walk the face of the earth."
    
    n2 "Sure, we didn't have the raw strength of a gorilla,{w}\n\nor the speed of a cheetah,{w}\n\nor the sharp claws of a tiger."
    
    n2 "What we have is that pink squishy thing nestled inside our skulls.{w}\n\nOur brain.{w}\n\nIntelligence.{w}\n\nGod's very own magnum opus."
    
    n2 "We managed to adapt and survive on all sorts of extreme conditions.{w}\n\nThriving in them.{w}\n\nUsing them to our advantage.{w}\n\nManipulating fire, water, land, and wind to serve our purpose."
    
    n2 "We created tools to augment our limbs.{w}\n\nAccomplishing feats impossible for lesser creatures.{w}\n\nCreating technologies indistinguishable from magic.{w}\n\nSlowly closing the gap between us and God."
    
    n2 "We invented language, a universal means of communication.{w}\n\nOur nature as a social creature extends from the need of survival as well.{w}\n\nHumans, by definition, are pack hunters.{w}\n\nApex predators banding together to serve a common purpose."
    
    n2 "These are our greatest weapons.{w}\n\nThings that makes us the most efficient killing machine the world has ever seen."
    
    n2 "And people can’t stop wondering why crime happens, why we wage wars, why we as humans just can’t stop killing each other.{w}\n\nIt's just our nature.{w}\n\nIt's in our blood."
    
    show card act2_0 with Dissolve(5.0)
    centered ""
    scene bg classroom with Dissolve(5.0)
    show card act2_1 with dissolve
    stop music fadeout 2.0
    play music "SFX/classroom.mp3" fadein 1.0
    centered ""
    hide card act2_1 with dissolve
    
    show leon speaking at right with easeinright
    l "And then she said that she’s on her period. Of course, that didn’t stop me, so I said-"
    
    show leon normal at right
    a "Shut it, the lecture’s starting."
    
    hide leon with easeoutright
    stop sound fadeout 3.0
    an "The chatter dies down as the professor enters the class."
    extend "\nI pull out my laptop, preparing to take notes, when something ugly rears it’s head."
    
    play music "BGM/Haunted.mp3" fadein 2
    an "From the depths of my bag, a slight glimpse of death."
    extend "\nLike the eyes of a beast, shining through the darkness."
    extend "\nI can see my gun reflecting the light of the room, as if to remind me of what’s coming."
    
    an "How?"
    extend "\nHow on earth did I get caught?"
    extend "\nWhere did I slip up?"
    
    an "It couldn’t be Dalton or The Syndicate."
    extend "\nIf it was, I’d be six feet under the ground instead of...{w} here."
    extend " Attending class like it’s nobody’s business."
    extend "\nIt’s... someone else."
    
    an "The police? Trying to turn me into an inside man?"
    extend "\nRivals? Trying to extract something out of me?"
    extend "\nReporters? Wildly chasing a story to tell the masses?"
    
    an "It could be anyone, anything. And currently, I have no way of fighting back."
    extend "\nHe... got me. He got me real good. I have no escape."
    extend "\nHe knows me. He knows what I’m doing. He knows about Anna."
    
    an "Anna."
    extend "\nHer death has never been publicized, The Syndicate covered up that incident neatly."
    extend "\nI was only questioned at the police station once."
    
    an "Then... it’s like she never existed in the first place."
    extend "\nGone, forgotten, erased."
    
    an "But this guy managed to uncover it, seeing the truth beneath the layers of lies and deceit."
    extend "\nDigging and clawing through the sea of corruption and bribery."
    extend "\nLaying the truth bare and feasting his eyes on it."
    
    an "Whoever he is, he’s capable. Terrifyingly so."
    extend "\nHe can’t be the police, or gangsters, or a reporter for that matter."
    extend "\nHe is something worse... much worse."
    
    an "My eyes turn back to the gun I was concealing in my bag."
    extend "\nI know that this thing won’t have much of a chance of saving me from this predicament."
    extend "\nBut, maybe it’ll even the odds...{w} hopefully."
    
    show leon speaking at right with easeinright:
        zoom 0.9
    l "Arthur."
    
    an "All I need is one chance."
    extend "\nJust one slip-up from him."
    extend "\nI can still win. I can still beat him."
    an "That is, assuming that he works alone."
    extend "\nOtherwise, I’m fucked."
    
    stop music fadeout 0.5
    show leon speaking at center with ease:
        easeout 0.5 zoom 1
    l "Hey, Arthur."
    
    a "Ah, what?"
    
    l "Class is over. Let's go."
    hide leon speaking at right with easeoutright
    
    an "Shit... I didn’t take notes."
    
    scene bg shipyard with Fade (2,0,2)
    show card act2_2 with dissolve
    play music "BGM/Secret.mp3" fadein 1.0
    centered ""
    hide card act2_2 with dissolve
    
    an "The coast seems to be clear."
    extend "\nHopefully Mr. Blackmailer doesn’t have any nasty surprises in store."
    an "Seems unlikely though, since he wants something from me."
    extend "\nOtherwise we won’t meet face to face like this."
    an "If the situation suddenly turn for the worse, my best chance of survival is to just escape."
    extend "\nRun to my car and drive away from here as fast as I can."
    an "Since it’s equipped with run flat tires and bulletproof glass, I have a pretty big chance of making it out of here."
    extend "\nWell, until my cover is blown by him and The Syndicate issued a fucking manhunt for me."
    an "I’ll end up dead either way."
    extend "\nMy only chance of settling this problem is only at this moment."
    extend "\nThe moment where we meet face to face."
    an "Where he’s the most vulnerable."
    extend "\nWhere I can see his flesh and blood, not behind computer screen and phone receivers."
    
    an "I only have a single handgun, loaded with 10 hollow point bullets."
    extend "\nOne good shot is all it takes, assuming that he isn’t wearing armor."
    extend "\nOne through the heart, one through the brain."
    an "Should be enough, unless he has some sort of immortality."
    extend "\nWho knows?"
    
    an "Suddenly, my phone vibrates."
    extend "\nI just received a text from a private number. Presumably the blackmailer."
    tm "Come outside, I’m near the pier."
    an "He’s been watching me all this time."
    
    a "Alright, then."
    
    stop music fadeout 2
    scene black with dissolve
    play sound "SFX/wind.mp3" loop fadein 1.0
    
    an "I walk outside, feeling the freezing night wind stinging my skin, as if the angels of death are blowing his cold lifeless breath right into my nape, signifying my soon-to-be death."
    an "Taking my gun out of its holster, I keep walking, nearer towards the unknown."
    
    an "Just around the corner, that monster awaits."
    extend "\nSafeties off, finger on the trigger, discipline be damned."
    an "Just around the corner, acquire target, aim for the head, pull the trigger."
    extend "\nEasy as 1-2-3."
    extend "\nHe’s only human, he can die like everyone else."
    stop sound fadeout 1
    an "I can see his shadow."
    extend "\nIt’s now or never!"
    scene bg gunpoint with Dissolve(0.5)
    
    play music "BGM/Revealed.mp3" fadein 1
    a_CG "{size=+30}!!!!!!!{/size}"
    
    an_CG "My hand froze, my whole body stops moving, as if bewitched by the Medusa."
    extend "\nMy mind screaming to my body to stop, to not pull the trigger."
    an_CG "A girl?"
    extend "\nWhat’s she doing here?"
    extend "\nWhy didn’t I just shoot her?"
    extend "\nI don’t understand what’s—"
    
    cSecret "Put the gun down if you’re not going to shoot me."
    
    an_CG "Her voice snaps me back from my daze."
    extend "\nI can start making sense of the situation."
    extend "\nShe’s… too calm for a girl being held at gunpoint."
    an_CG "She’s not normal."
    an_CG "Is she the blackmailer’s lackey?"
    extend "\nDid that fucking asshole used a girl to come face to face with me?"
    
    a_CG "Where’s the guy who blackmailed me?"
    
    cSecret "You’re looking at her. I’m the one who sent you that email."
    
    an_CG "Unbelievable, is this a fucking joke?"
    extend "\nThey did say not to judge a book by it’s cover but this is just taking it too far."
    an_CG "Should I kill her?"
    extend "\nWhat if she’s lying, and there’s in fact a mastermind behind this?"
    extend "\nI should ask more questions before pulling the trigger."
    
    a_CG "Are you the one that’s behind all of this?\nDigging around for my info and blackmailing me.\nIs this all your doing?"
    
    cSecret "What can I say to convince you?\nI’m the one that did it.\nBut, you still wouldn’t believe me, right?"
    
    a_CG "Assuming you’re the one that did it, why?\nWhat is your objective?"
    
    cSecret "I have a proposition for you."
    extend "\nBut you better put the gun down, unless you want your head to be blown to pieces by a sniper."
    extend "\nYou have...{w} ten seconds."
    
    a_CG "Bluffing won't get you anywhere."
    
    cSecret "Suit yourself, at least I warned you."
    
    an_CG "I can't tell if she's bluffing or not."
    extend "\nFuck, she's a monster alright."
    
    scene bg gunpoint2 with dissolve
    
    a_CG "Spit it out, I'm listening."
    
    cSecret "Right."
    extend " I’m going after The Syndicate, just like you."
    extend " I want us to help each other out."
    
    a_CG "Why should I trust you?"
    
    cSecret "Sadly, I can’t offer you anything else to prove myself trustworthy other than ‘Our interests aligned’."
    
    a_CG "Try harder."
    a_CG "What skills do you possess?"
    extend "\nWhy do you need me?"
    extend "\nWhy should I help you?"
    
    cSecret "I can get you information, and not just deep web sources that you’ve been using all this time."
    extend "\nBetter source, better credibility."
    cSecret "I can procure equipment; cars, guns, bombs."
    extend "\nYou name it, I’ll have it."
    
    cSecret "Besides, do you really believe that you can take on The Syndicate by yourself?"
    extend "\nI thought you’re smarter than that."
    cSecret "You know full well what they’re capable of, there is no way you can take them on alone."
    cSecret "All you’ll accomplish is ending up dead. That’s counterproductive."
    
    an_CG "..."
    
    an_CG "She’s right, as much as I hate to admit it."
    extend "\nI can’t win."
    an_CG "She probably is my best shot at taking them down, if what she’s saying about her capabilities is true."
    an_CG "On the other hand, that same capability can be used to take control and manipulate me into doing her biddings."
    
    an_CG "For now, I don’t see any other choice."
    extend "\nWe’ll just take advantage of each other for now, see where it leads us."
    
    a_CG "Alright, we have a deal."
    extend "\nFrom this point on, we’re allies."
    a_CG "But, if I caught even a little glimpse of you trying to do anything funny, I won’t hesitate to eliminate you."
    
    cSecret "The same can be said for you, Arthur."
    extend "\nNo funny business, no backstabbing me."
    extend "\nDon’t even think of trying to betray me, I’ll know."
    
    a_CG "Duly noted."
    extend "\nSo, do you have some sort of plan?"
    
    cSecret "We wait."
    extend "\nYou can go home now, contact me if something came up."
    extend "\nWe’re done here."
    
    a_CG "Wait."
    
    cSecret "What is it?"
    
    a_CG "Tell me your name."
    
    cSecret "Ciel Blanchette."
    extend "\nYou’re free to go and dig around for my info in the deep web."
    extend "\nI’m sure you’ll find something."
    
    stop music fadeout 2
    scene black with dissolve
    
    an "She turn on her heels and walks away."
    extend "\nHer small figure taking firm steps even while being battered by the night gust."
    an "Like a young sapling in a middle of a storm, swaying back and forth, standing it’s ground against the onslaught of mother nature."
    an "Her image seems...{w} lonely, somehow."
    
    an "Seeing that she already disappeared around the corner, there’s not much point for me to stand here."
    an "It’s cold."
    
    show card act2_3 with dissolve
    centered ""
    hide card act2_3 with dissolve
    
    play sound "SFX/brake.mp3"
    an "Arriving at a stoplight, I shift my car into neutral and kick back, waiting for the light to turn green."
    
    an "Ciel."
    extend "\nAs it stands, the situation looks pretty disadvantageous to me."
    an "She’s the one holding all the cards."
    extend "\nShe knows me, she knows my past, she knows how I operate, she knows how I think."
    an "I don’t know anything about her."
    extend "\nI have no leverage over her."
    extend "\nI can’t put a firm grasp on her."
    
    play sound "SFX/bikestop.mp3"
    an "Then, I hear sounds."
    extend "\nThe sound of a high-revving-small-displacement engine, coming to a stop beside my car."
    extend "\nA red sport bike, with a girl riding on it."
    
    an "A smile formed on my face."
    extend "\nI start giggling by myself, laughing at the idea that immediately crossed my mind when I saw her."
    an "A wildcard."
    extend "\nThe equalizer."
    an "The best - and simultaneously the worst - solution to my problem. Adding this vigilante to the mix should yield an interesting result."
    
    an "It all came together so suddenly."
    extend "\nCiel and The Vigilante."
    extend "\nDalton and Reyes."
    extend "\nMe and The Syndicate."
    an "The pieces of puzzle instantly come into place, and the path to victory suddenly lit up before me."
    extend "\nCiel and this girl, they are the key."
    an "Taking down The Syndicate is no longer an impossible task."
    extend "\nI can do it!"
    
    scene bg bike with dissolve
    play music "BGM/pursuitedge.mp3"
    play sound "SFX/cargo.mp3"
    
    an_CG "The green light lit up, her cue to take off, spreading justice in the city of Wolfsburgh."
    extend "\nI have to give chase, I still have to see her capabilities first-hand."
    an_CG "I stomp on the throttle, launching my car from a standstill."
    extend "\nThe acceleration pins my back to the seat, shifting gears as the needle reaches the 7000 rpm redline on the rev-counter."
    
    an_CG "Finally, I get to use the horses of this German performance coupe."
    an_CG "As she blasts through the streets, I try to keep up while also maintaining a two car distance from her."
    an_CG "Her majestic steed weaves through the corners of the city streets, slowly increasing the gap between us."
    
    an_CG "Guess I’ll have to get a bit more ‘extreme’ with my cornering then."
    an_CG "As she zips around yet another corner, I initiated a heel-toe downshift, shifting the weight of the car to the front, unloading the rear tires."
    an_CG "As the understeer prevents the car from changing direction, I apply just enough throttle to break the rear tires traction, rotating the car into the corner"
    an_CG "Catching the oversteer with some opposite-lock, I stabilize the car and fire off into the straights, unleashing all of the 550 horsepower onto the rear wheels."
    extend " Tracing the path carved by the red devil."
    an_CG "Watching the tachometer needle dancing back and forth, following the rhythm of my driving."
    
    scene black with dissolve
    
    centered "15 minutes later"
    
    play sound "SFX/carstop.mp3"
    an "Seemingly catching her eyes on something, she slam on the brakes."
    extend "\nShe stops in front of an alley, jump off her bike, and slowly walks into it."
    an "Losing track of her, I decide to park my car some distance away and get out, trying to get a better view."
    
    scene bg alley 1 with dissolve
    show julie 6 at right with easeinright
    an "There are three thugs beating up some poor guy to a pulp."
    extend "\nAnd it seems that they haven’t noticed her calmly walking towards them."
    an "Looks like shit’s about to go down."
    extend "\nShame I don’t have a bowl of tasty popcorn at hand."
    
    show julie 5 at right
    vig "What the fuck are you guys doing?"
    
    thug1 "Hmm? Well, it’s none of your fucking business lady, now move along…"
    
    show julie 8 at right
    vig "....."
    
    show julie 2 at right
    vig "I SAID WHAT THE FUCK ARE YOU GUYS DOING?! LET THAT GUY GO!"
    
    show julie 6 at right
    thug2 "Hey, what the fuck is this crazy bitch on about?"
    
    thug3 "Well lady, we got some unfinished business with this little guy here. So why don’t you{w=0.4} FUCK{w=0.4} OFF?!"
    
    thug2 "Or, she could be tempting us. Ehehehe..."
    
    thug1 "“Ah... that makes sense. The little lady wanted to have some fun with us, huh?!"
    
    vig "Oh God... You people are fucking idiots."
    
    thug2 "Hey, watch your language, lady. We might punish you for that."
    
    vig "Hey, you there, yeah, you, Mr. Victim. Get out of here. I’ll take care of these morons."
    
    vic "B-but...{w} but I..."
    
    show julie 2 at right
    vig "JUST GO!!!"
    
    show julie 6 at right
    vic "Hiiii! I’m sorry… I’m so sorry…"
    
    thug1 "Hey, he fucking escaped."
    
    thug3 "Let him be, we’ve got ourselves a new toy for now."
    
    thug2 "That guy owe us some money lady. Now you’ll pay for it, with your body of course. Ahahahahaha!!!"
    
    hide julie with easeoutright
    n2 "She probably could feel their eyes on her. Crowding around, the thugs inches closer and closer, like beasts cornering it’s prey. With her back against the wall, she has nowhere to escape now.{w}\n\n{i}sigh{i}{w}\n\"You men are all the same, aren't you? So easy.\""
    play sound "SFX/slamcrotch.mp3"
    with vpunch
    $ renpy.vibrate(0.25)
    extend "\n\n\"---Guh!\"{w}\nBefore the thugs had realized what happened, she already put her knee into his crotch, crushing his testicles. The thug slowly crumbles unto the ground, writhing in pain.{w}\n\nShe put her fists up, now revealed to be armed with brass knuckle, preparing to throw the next punch."
    
    n2 "\"Come on people, I ain’t got all night.\"{w}\nHer lips starts to curve into an expression best described as excitement.{w}\n\nThe thugs, just registering the fact that she just downed one of their friend, turn their attention to her and realized the situation that they were in.{w}\n\n\"What the f—\"\nToo slow. With her thumbs now buried deep within his eye socket, he’s as good as gone.{w}\n\"AAAAAAAAGGHHHHH!!!!\"{w}\nWith the guy blinded, she pulls back for the final blow."
    play sound "SFX/spinkick.mp3"
    with hpunch
    $ renpy.vibrate(0.25)
    extend "\nA roundhouse kick to the side of the face did the job, twisting his head, and subsequently his body. Concussion guaranteed."
    
    n2 "With the guy on the left knocked out, the only one left is now standing to her right, slightly out of her field of vision.{w}\n\nAs she turn her head towards him, her eyes caught a glimpse of his hand, the unmistakable glint of a sharp metal. He’s pulling out a knife!{w}\n\n\"Like I’ll let you!!—\"{nw}"
    play sound "SFX/uppercut.mp3"
    with vpunch
    $ renpy.vibrate(0.25)
    extend "\nIn a blink of an eye, she closed the gap between them to a mere few centimeters. She drives her right fist upwards, landing it squarely onto his chin, breaking his teeth and shattering his jaw. The crunching sound of broken bones echoes through the alley, signaling the end of this street fight."
    
    n2 "As the last guy falls, she check her surroundings in case any of them try to attack her again. She relaxed her body as the coast seems to be clear.{w}\n\n'It all happened so quickly, I can’t believe it,' Arthur thought in disbelief.{w}\n\nA true demonstration of a near superhuman reaction time.{w}\n\nHer perfectly executed move, probably honed from many years of training, dispatched them before they even had a chance to attack.{w}\n\nIt’s… fascinating, how an act so violent and brutal can be executed with such an amazing efficiency and grace. Her vicious bloodlust combined with her precision and speed would make any grown men think twice about crossing her."
    
    stop music fadeout 1
    show julie 6 at right with easeinright
    vig "Fuuh... Next time please don’t pull out a weapon. You’ll make me forget to hold back."
    extend "\nHey! are you liste– oh. He’s in shock. Oh well."
    
    vig "Heh, looks like you’ll be eating with a straw for a while. That’ll teach you a lesson."
    
    scene black with dissolve
    
    an "I’ve seen enough. Time to get out of here before she notices me."
    
    an "Tomorrow night is going to be very interesting."
    
    scene bg alley 2 with dissolve
    show card act2_4 with dissolve
    centered ""
    hide card act2_4 with dissolve
    
    play music "BGM/sweetrevenge.mp3" fadein 1
    gang1 "WHERE"
    
    show red:
        alpha 0.5
    play sound "SFX/slamcrotch.mp3"
    with vpunch
    $ renpy.vibrate(0.25)
    show red:
        alpha 0.5
        linear 0.5 alpha 0.0
    
    gang1 "IS"
    
    show red:
        alpha 0.5
    play sound "SFX/slamcrotch.mp3"
    with vpunch
    $ renpy.vibrate(0.25)
    show red:
        alpha 0.5
        linear 0.5 alpha 0.0
    
    gang1 "MY"
    
    show red:
        alpha 0.5
    play sound "SFX/punch2.mp3"
    with vpunch
    $ renpy.vibrate(0.25)
    show red:
        alpha 0.5
        linear 0.5 alpha 0.0
    
    gang1 "FUCKING"
    
    show red:
        alpha 0.5
    play sound "SFX/spinkick.mp3"
    with hpunch
    $ renpy.vibrate(0.25)
    show red:
        alpha 0.5
        linear 0.5 alpha 0.0
    
    gang1 "MONEY?!"
    
    show red:
        alpha 0.5
    play sound "SFX/uppercut.mp3"
    with vpunch
    $ renpy.vibrate(0.25)
    show red:
        alpha 0.5
        linear 1.5 alpha 0.1
    queue sound "SFX/heartbeat1.mp3" loop
    a "{i}cough{/i} {i}cough{/i}"
    extend "... I'm sorry... please..."
    stop sound
    
    hide red
    gang2 "We don’t need your fuckin’ excuse. Call your parents and beg them for it!"
    
    gang3 "We’re wastin’ our time. Let’s just take whatever he has. His shoes looks pretty expensive, no?"
    
    gang2 "Whoa, no need to rush. I haven’t had a shot at him yet. Relax."
    
    an "The second guy approaches me whilst cracking his knuckles."
    
    gang2 "Really sorry about this bro… You should’ve listened to us. Now, Imma use you for a lil’…{w} exercise."
    
    show red:
        alpha 0.5
    play sound "SFX/slam.mp3"
    with vpunch
    $ renpy.vibrate(0.25)
    a "KAAGHHH!!"
    
    show red:
        alpha 0.5
        linear 1.0 alpha 0.1
    gang2 "Oooooh daaamn. That one’s pretty solid. How ya feelin', man?"
    
    an "He drove his fist right into my stomach. Causing the contents to rise back up to my throat. I proceed to throw up all over the concrete pavement of this alley."
    
    gang1 "Oh man, you just spilled your lunch all over the ground. Here, let me HELP!!!"
    
    an "He pulled me up by the hair and planted my face on the puddle of my own barf, then decide to stomp on my head for cheap laughs."
    
    gang1 "Your parents taught you not to waste food right? Now, you’re going to lap up everything that you just wasted. Come on now, don’t be shy."
    
    an "..."
    an "I think I'll cut their wages this month."
    
    a "...HEEELP MEEEEE!!!..."
    
    hide red
    gang3 "Ahahaha!!! Seriously? You better get up and fight then, 'cuz no one’s comin' for you."
    
    show red:
        alpha 0.5
    play sound "SFX/slam.mp3"
    with vpunch
    $ renpy.vibrate(0.25)
    gang2 "GET SOME!!!"
    
    show red:
        linear 1 alpha 0
    show black:
        alpha 0
        linear 1 alpha 0.5
    an "That last blow just sapped all the strength I have left."
    extend "\nI grit my teeth as my vision gets darker."
    an "I can’t lose consciousness.{w} Not yet.{w} Not.....{w} yet."
    
    an "...I've got to—"
    
    vig "You motherfuckers!!!"
    
    gang2 "What the fuck?!"
    
    gang1 "SHIT!!"
    
    an "Heheh. There she is."
    
    show black zorder 2:
        alpha 0.5
        linear 1.0 alpha 1.0
    stop music fadeout 1
    extend "\nNow…{w} I can slee—"    
    
    play sound "SFX/punch.mp3"
    an "...."
    play sound "SFX/uppercut.mp3"
    an "...."
    an "...."
    vig ".............up."
    an "...."
    vig ".....me on.........."
    
    show julie 9 zorder 1 with easeinbottom:
        zoom 1.5
        yalign 0
        xalign 0.5
    show black zorder 2:
        alpha 1
        linear 0.5 alpha 0.85
    play sound "SFX/wind.mp3" loop
    an "What?"
    
    vig "Come on, wake up."
    show black zorder 2:
        alpha 0.85
        linear 0.5 alpha 0.7
    an "Is it her? She…{w} took everyone out already?"
    
    a "No, please… don't hurt me…"
    
    show black zorder 2:
        alpha 0.7
        linear 0.5 alpha 0.55
    vig "Calm down. No one’s going to hurt you now. I took care of them."
    
    an "It is her. Good."
    
    a "Who... are you?"
    
    j "My name’s Julie. I’m here to help, don’t worry."
    
    j "Can you get up? Come on, I'll help you."
    
    hide black with dissolve
    show julie 9 at right zorder 1 with ease:
        ease 0.3 zoom 1
    an "As my vision recovers, I can start to see the damages she dealt to the guys."
    an "On the ground lays a brick covered in blood. Coincidentally, one of the guys now has a nasty gash on his scalp still leaking fresh blood."
    
    a "What have you done to these guys?"
    
    show julie 1 at center with ease
    j "Just giving them a taste of divine punishment. A pretty fitting end for them, wouldn’t you say?"
    
    a "Isn’t this a little…{w} overdoing it?"
    
    j "Nah… they’ll live…"
    extend "\nBy the way, I haven’t heard your name yet."
    
    a "Arthur Woods. Thanks for saving me by the way. I owe you big time."
    
    j "Don’t sweat it. Right now, we need to get you home safely."
    extend "\nCome on, I’ll help you walk."
    
    j "We look the same age, Arthur. Are you a college student?"
    
    a "Yeah, I go to Iris University, business major."
    
    j "I knew it. What a small world we live in, huh? I also go to Iris, currently studying Psychology."
    j "Here, give me your arm."
    
    a "Ahh, thanks. It’s not that far, you can see the building from here."
    a "Really sorry for this, I promise I’ll make it up to you."
    
    stop sound fadeout 2
    scene bg livingroom night with fade
    show card act2_5 with dissolve
    centered ""
    hide card act2_5 with dissolve
    
    play music "BGM/casual.mp3" fadein 1
    show julie 1 with easeinright:
        zoom 1.3 yalign 0 xalign 0.5
    j "Wow, nice place you got here."
    
    extend "\nOOH! Is that a VS4 game console? Can I play?!"
    
    a "Yeah sure, go ahead and play. I’ll just go clean myself up for a moment."
    
    j "Alright!"
    
    show julie:
        ease 0.3 zoom 1
    an "She excitedly run up to the game console and turn it on. While she’s busy playing, I can go clean my own vomit off my face."
    
    scene black with dissolve
    centered "15 minutes later"
    scene bg livingroom night with dissolve
    
    show julie 5 with dissolve
    j "I’ll… take that shotgun, thank you."
    
    an "Julie is currently engrossed in her games."
    extend "\nI should make some small talk with her."
    
    a "Why not stick with the SMG? Higher fire rate, faster reload, have a bit more {i}penetration{/i}."
    
    j "Well, I actually prefer a bit more spread, since we’re fighting close-quarters."
    
    a "Ah, you like a bit more {i}spread{/i} huh? I guess I can get {i}behind{/i} that."
    
    show julie 5 
    j "If you don’t stop with the sexual innuendo, I’m gonna tear your dick off."
    
    an "Needless to say, I immediately banish any thoughts of making another one."
    extend "\nIn hindsight, she just took down three burly gangsters in under a minute. Showering her with sex jokes is {i}such{/i} a good idea."
    
    j "By the way Arthur, can you tell me why those guys are attacking you?"
    
    a "They want my money, I guess. They’ve been at it for the past couple of months, since they robbed me that night."
    a "I do realize that I’m an easy target, and truthfully, I have considered using pepper spray and hiring bodyguards."
    a "But… I just can’t do it. Just the thought of inflicting pain to other people makes me feel sick."
    a "You know, maybe if we talk to them—"
    
    an "She suddenly paused her game and stared me straight in the eye. Like she’s peering through my soul."
    
    j "You’re too soft, Arthur, too kind for your own good. I guess it’s a good thing since there are not many people like you left in this shitty world."
    j "But you have to realize, not all people have good intentions or goal in their mind."
    j "Some will just take from others for their own selfish reasons. Some will…{w} hurt other people just for the sake of it."
    
    a "Yeah… I guess you’re right. I’m sorry. I guess I’ve been naïve."
    
    j "Alright, listen to me Arthur."
    extend "\nYou’re a good man. You don’t deserve this kind of treatment from others."
    j "From now on, if anybody ever threatens you or try to hurt you again, call me immediately."
    extend "\nI’ll protect you, I promise."
    a "…Is that a confession of love?"
    
    j "Do you want to die?"
    
    a "No, I'm sorry..."
    
    j "Anyway, if you by any chance had a feeling that you’re going to be attacked again, call me immediately. Got that?"
    
    a "R...roger."
    
    j "Good."
    extend "\nNow, I’m going to go home, I have class tomorrow."
    extend "\nYou, keep an eye out for yourself.{w} And don’t delete my save."
    
    an "She quickly gathered her stuff and leave, glancing at me one more time before closing the door behind her."
    
    hide julie with easeoutleft
    stop music fadeout 2
    an "..."
    an "..."
    play music "BGM/secret.mp3" fadein 1
    an "..."
    
    a "Wow. She bought it hook, line, and sinker."
    
    an "Well, at least it’s worth getting beat to a pulp."
    extend "\nNow, with the wildcard in my hand, I can start putting down my chips."
    extend "\nNow, I can play."
    
    stop music fadeout 2
    scene bg bedroom morning with fade
    show card act2_6 with dissolve
    centered ""
    hide card act2_6 with dissolve
    
    play music "BGM/secret.mp3" fadein 1
    play sound "SFX/keyboard.mp3"
    a "So you’re saying she’s clean?"
    
    inf "Yeah, no history of violence, no family killed, graduated high school with excellent scores. She’s your typical goody two shoes, this Ciel.{w} But…"
    
    a "But what?"
    
    inf "I dunno. Perhaps it’s too clean… if that makes any sense. It feels like looking at a picture of a hamburger in a fast food joint - if you know what I mean."
    inf "I dunno man, something about it disturbs me. Maybe it’s my experience as an information broker, but, I wouldn’t trust anything I just found here."
    
    a "So…{w} you’re saying that everything you found was…{w} fabricated in some ways?"
    
    inf "…{w}Possibly yes. But, it’s just a gut feeling though."
    
    a "Theoretically, if these data you found was indeed fabricated, what kind of person would you think she is? How would she accomplish such thing? And what else she’s capable of doing?"
    
    inf "If I had to guess, and bear with me here, because it’s pretty ‘out there’, I… would guess that she’s a member of an intelligence agency."
    inf "They’re the experts of tampering with someone’s identity, erasing people out of existence, and making up non-existent ones. One would think that she’s the subject of such treatment."
    
    a "Lay off the tinfoil hat, dude. This is not the time for jokes."
    
    inf "I’m serious. If I were you, I’d be treading very carefully from now on. You’re one of my biggest client after all, would be a shame if you died."
    
    a "Right… that’s all you care about right?"
    
    inf "Hey, I’m just an honest man doing an honest day work."
    
    a "Alright then. Your honest day’s salary is being wired to your account right now."
    extend "\nAnd if you find anything about her that seems important, inform me immediately."
    
    inf "Sure thing, boss man."
    
    an "I ended the call and fall back to my chair."
    
    an "Assuming that the informant’s instinct is correct, I may have gotten myself in some really deep shit. It would imply that The Syndicate’s circle of influence is much larger than I originally thought."
    an "I might end up butting heads with some alphabet agency."
    
    an "Man, what a horrible way to start a day. Getting warned of the dangers of causing an international incident isn’t really high on my list of ‘Things I want to hear when plotting to destroy a multinational crime syndicate.’"
    
    a "Then again, does it really matter? It’s not like I—"
    
    play sound "SFX/handphonering.mp3"
    an "Suddenly, my phone rings and vibrates. There’s a call from a private number."
    extend "\nI reluctantly pick it up."
    
    c "How’s the information you found? Got any leverage over me?"
    
    an "See!? I knew it."
    
    a "What do you want?"
    
    c "Yeah… we need to meet up. I need to discuss something with you."
    
    a "Alright, come down to my apartment then."
    
    c "Tsk tsk tsk. Luring me into a trap isn’t going to be that easy Arthur…{w} especially when the guy inviting me is as dangerous as you."
    c "Come down to the boulevard. I’ll find you once you’re there."
    
    a "Alright."
    
    stop music fadeout 2
    scene bg vestra day with fade
    show card act2_7 with dissolve
    centered ""
    hide card act2_7 with dissolve
    play music "BGM/Flee.mp3" fadein 3
    
    show black:
        alpha 0.2
        linear 1.0 alpha 0.3
    an "Well, here I am. In a middle of a busy boulevard with Ciel nowhere in sight. I guess I’ll have to wait then."
    
    an "..."
    
    an "Look at all these people. Living their lives, blissfully ignorant of how the world actually works."
    
    an "Land a good job, get a steady income, create a happy family they say. History has proven that this is the recipe to a happy and fulfilling life."
    
    an "Unfortunately, some people in this world just got dealt a bad hand."
    extend "\nSome were born poor. Some never received a proper education. Some lived on the streets all their lives."
    
    an "These people get desperate sometimes. So they turn into the underworld - the dark side, in order to improve their chances at life. A last ditch effort on a happy and fulfilling life. And many of them didn’t make it."
    
    an "I’ve seen how many lives got ruined by the promise of easy money."
    
    an "With that in mind, what the hell am I doing here? I had a good life. I’m not poor. Yet I plunged myself into this hellhole for—"
    
    hide black
    stop music
    play music "SFX/crowd.mp3"
    show ciel 6 with easeinleft
    c "Stop daydreaming, mister."
    
    a "Ciel. Where the hell have you been?"
    
    show ciel 5
    c "Me? I just returned from the toilet."
    
    a "I see."
    
    show ciel 2
    c "Speaking of which…"
    
    show ciel 2 at left with ease:
        easein 1 zoom 1.3 yalign 0
    an "Ciel suddenly linked my arm with hers."
    
    c "Lets go on a date while we’re here!"
    
    show ciel 5:
        ease 0.5 zoom 1 xalign 0.5 yalign 0
    a "Don’t get too close to me. You probably haven’t washed your hands yet."
    
    show ciel 8
    c "Why? Do you hate cute girls like me?"
    
    a "Of course not. I love cute girls. I adore cute girls. Someone pretending to be one on the other hand…"
    
    show ciel 1
    c "Don’t be so harsh on me… This is my real personality."
    
    a "Whatever you say, darling."
    
    c "Thank you…"
    extend "\nYou’re seem to be quite the lady killer, Arthur. You must have many ‘experience’ with women, huh?"
    
    a "Nah… I’m still a virgin."
    
    show ciel 4
    c "Oh really?"
    
    a "Yeah. I never had sex with a woman before."
    
    c "Why? I mean, it feels pretty good you know…"
    
    stop sound
    show bg vestra mono
    show ciel mono
    play sound "SFX/kaget.mp3"
    a "I suffer from a severe erectile dysfunction."
    
    show bg vestra day
    show ciel 9
    c "..."
    
    a "..."

    show ciel 7
    c "I’m sorry. I’m truly sorry. I did not mean to offend you in any way."
    
    a "That was a joke, you know?"
    
    show ciel 5
    c "O…{w} Oh? It’s a joke huh…{w} Aha…{w} ahahahaha, nice joke Arthur… I-{w}I swear I’ll never tell this joke to anybody else, I promise…{w} But yeah! You had me there…{w} yeah…"
    
    an "Seems like I just created a huge misunderstanding on her part. Oh well."
    
    play music "BGM/trailer2.mp3" fadein 1
    a "Alright, enough of joking around. Why am I here?"
    
    show ciel 2
    c "Yeah… {i}ahem{/i}{w} I forgot to tell you something yesterday."
    extend "\nUmm...{w} basically, your life is in danger, Arthur."
    
    a "So what? Lots of people want me dead. This isn’t new."
    
    show ciel 1
    c "No, he’s different. A true pure-bred specially trained assassin, with a personal vendetta against you. I suggest that you abandon your apartment and change your daily routine."
    
    a "I’ll keep that in mind. Right now, I’d rather focus on tracking down the leader of The Syndicate."
    
    show ciel 2
    c "Don’t say that I didn’t warn you…"
    extend "\nSo, what have you got for me?"
    
    a "You are familiar with Milton Corp. don’t you? One of The Syndicate’s shell company."
    
    show ciel 1
    c "Your workplace. What of it?"
    
    a "I need some help to climb the corporate ladder. I need to own Milton Corp. so that I can come in contact with the people in higher places, and eventually, the leader."
    
    c "Alright, we need to lay down the groundwork then."
    extend "\nWe’ll need a ma—"
    
    a "Ah, I’ve already done the prep work. Most of the pieces of the puzzle are in place. All that’s left to complete it, is the two of you."
    
    show ciel 8
    c "What do you mean… ‘The two of you’?"
    
    stop music fadeout 1
    scene bg mobius day with fade
    show card act2_8 with dissolve
    centered ""
    hide card act2_8 with dissolve
    play music "SFX/cafenoise.mp3"

    show ciel 1 at left with easeinleft
    a "Remember, just play along. Maybe we’ll get some Oscar for best acting after this."
    
    show ciel 3 at left
    c "You already told me the same thing 8 times, just in a slightly different way.{w} I{w=0.2} GOT{w=0.2} IT."
    
    a "Okay, okay... Just checking."
    extend "\nBy the way, I haven’t mentioned you when I called her so make sure you introduce yourself properly."
    a "And don’t crack any weird jokes at her, she might tear your dick off."
    
    show ciel 4 at left
    c "Hey, is that her over there? The short-hair-black-jacket-bitchy-resting-face girl?"
    
    a "Yup, that’s her alright. Now get your game face on, I’ll wave at her."
    
    show ciel 1 at left
    show julie 1 at right with easeinright
    j "Yo! Who’s your friend?"
    
    show ciel 6 at left
    c "Hi, My name is Ciel Blanchette. An exchange student from France. Nice to meet you."
    
    an "God…damnit.{w} Don’t add unnecessary stuff…{w} Just…{w} Oh god…"
    
    show julie 4 at right
    j "Oh really? That’s so cool! Can you try speaking in French, Ciel?"
    
    show ciel 8 at left:
        easein 0.3 zoom 1.3 yalign 0
    show julie 1 at right
    an "Why? Why are you looking at me? It’s your own fault, don’t come running to me for help."

    show ciel 7 at left:
        easein 0.3 zoom 1 yalign 0
    c "Umm… Oui oui baguette? Foie gras omelette du fromage danke danke."
    
    show julie 6 at right
    an "Deep breaths… Nothing funny here... Nothing at all..."
    
    show ciel 9 at left
    show julie 1 at right
    j "Sounds poetic! Like some kind of prose! What does it mean?"
    
    show ciel 9 at left:
        easein 0.3 zoom 1.3 yalign 0
    an "Don’t look at me again! I can’t help you! You brought this unto yourself. Deal with the consequences."
    
    show ciel 5 at left:
        easein 0.3 zoom 1 yalign 0
    c "It’s an… old expression... About a chef’s honor… I think…"
    
    a "Yeah, Ciel here actually used to work in a pastry shop in France, so she gets all of this weird knowledge of French cooking witchcraft. Pretty awesome, huh?"
    
    show julie 3 at right
    j "Oh! I see! That’s awesome!"
    
    show ciel 8 at left:
        easein 0.3 zoom 1.3 yalign 0
    an "…What?! I’m backing you up here, and now you look at me like that?! Fine! Have it your way! I don’t care anymore."
    
    show ciel 8 at left:
        easein 0.3 zoom 1 yalign 0
    show julie 5 at right
    a "Hey Ciel, didn’t you have an appointment with your friends?"
    
    c "A-Ah of course! Haha, you’re right! I-I’ll be off then. Arthur, call me later and tell me more about Julie okay! Bye bye!"
    
    hide ciel with easeoutleft
    j "She’s… pretty unique alright. Anyway, Arthur."
    
    a "Hm? What?"
    
    j "Did the guys from yesterday come to you again?"
    
    a "Umm yeah…{w} actually, that’s why I called you here. They…{w} they wanted to settle the score with you. They uh…{w} they are threatening to burn down my apartment if you didn’t come…{w} I’m sorry…"
    
    j "Alright… Where are they, then?"
    
    a "They said they’ll be waiting for you…{w} under the bridge…{w} near the park."
    extend "\nUhh…{w} y-you don’t have to actually go if you don’t want to, I can—"
    
    j "I’ll go, those bastards need someone to beat some sense into their head anyway. I’ll give them a little taste of hell."
    
    stop sound
    scene bg street with fade
    show card act2_9 with dissolve
    centered ""
    hide card act2_9 with dissolve
    play music "BGM/incoming.mp3" fadein 1
    
    gang1 "Hey, where’s Arthur? He said there’s an emergency."
    
    gang3 "I dunno man, I’ve been waiting for probably 15 minutes here. I haven’t seen him yet."
    
    an "I’m already here though…"
    
    gang2 "He’s probably salty about yesterday. I think we’ve gone a bit overboard there."
    
    an "No… Not at all… Why would I be angry? Making me throw up and telling me to lap it all up again? That’s not going overboard at all…"
    
    gang3 "Hey, I got 12 stitches on my shin. It’s a fair job, deal with it. We got a healthy compensation out of it as well."
    
    gang1 "Yeah man, check out my new golden teeth, pretty sweet huh? Not going to get this working under Dalton y’know."
    
    an "Yeah, fuck financial responsibility, amirite?"
    
    gang3 "Uhuh, that bitch knocked out four of your teeth huh?"
    
    an "Alright, enough of this. Let’s get it over with."
    
    gang2 "Y’know, I’ve been wonderin’, why did the bo—{nw}"
    play sound "SFX/silencergun.mp3"
    extend ""
    
    gang3 "Oh shit!—{nw}"
    play sound "SFX/silencergun.mp3"
    extend ""
    
    gang1 "Wha—{nw}"
    play sound "SFX/silencergun.mp3"
    extend ""
    
    an "Right…"
    extend "\nClean up loose ends - check."
    extend "\nAll that’s left is Reyes’s guys. She should be out there right now, doing her ‘community service’. Dalton’s in for some surprise…"
    
    stop music fadeout 1
    scene black with fade
    show card act2_e with dissolve
    centered ""
    hide card act2_e with dissolve
    
    # TODO
    # save progress here

    jump act3
    return