# Declare images below this line, using the image statement.
# eg. image eileen happy = "eileen_happy.png"
# background image. format: image bg <varname> = "<filepath>"
# filepath relatif terhadap direktori game

image splash = "UI/splash.png"
image splash1 = "UI/splash1.png"
image thankyou = "UI/thankyou.png"

image bg bedroom morning = "BG/BG2a.png"
image bg bedroom night = "BG/BG2.png"
image bg livingroom morning = "BG/BG5b.png"
image bg livingroom night = "BG/BG5a.png"
image bg miltonhq = "BG/BG4.png"
image bg shipyard = "BG/BG9.png"
image bg mobius day = "BG/BG11.jpg"
image bg mobius sepia = im.Sepia("BG/BG11.jpg")
image bg gunpoint = "BG/CG-2a.png"
image bg gunpoint2 = "BG/CG-2.png"
image bg bike = "BG/CG-1.png"
image bg classroom = "BG/BG13.png"
image bg vestra day = "BG/BG12a.png"
image bg vestra mono = im.Grayscale("BG/BG12a.png")
image bg vestra night = "BG/BG12b.png"
image bg alley 1 = "BG/BG10.png"
image bg alley 2 = "BG/BG8.png"
image bg street = "BG/BG6.png"
image bg bridge under = "BG/BG7.png"
image bg hideout night = "BG/BG3a.png"
image bg white = "#FFF"
image white = "#FFF"
image red = "#F00"

# character image. format: image <nama> <ekspresi> = "<filepath>"
image leon normal = "Chars/le_0003_pose-1.png"
image leon speaking = "Chars/le_0004_pose-2.png"
image dalton normal = "Chars/ma_0000_pose-1.png"
image dalton speaking = "Chars/ma_0001_pose-3.png"
image dalton grin = "Chars/ma_0002_pose-2.png"
image julie 1 = "Chars/j_0001_pose-1.png"
image julie 2 = "Chars/j_0000_pose-2.png"
image julie 3 = "Chars/j_0002_pose-3.png"
image julie 4 = "Chars/j_0003_pose-4.png"
image julie 5 = "Chars/j_0004_pose-5.png"
image julie 6 = "Chars/j_0005_pose-6.png"
image julie 7 = "Chars/j_0006_pose-7.png"
image julie 8 = "Chars/j_0007_pose-8.png"
image julie 9 = "Chars/j_0008_pose-9.png"
image ciel 1 = "Chars/c_0000_pose-1.png"
image ciel 2 = "Chars/c_0001_pose-2.png"
image ciel 3 = "Chars/c_0002_pose-3.png"
image ciel 4 = "Chars/c_0003_pose-4.png"
image ciel mono = im.Grayscale("Chars/c_0003_pose-4.png")
image ciel 5 = "Chars/c_0004_pose-5.png"
image ciel 6 = "Chars/c_0005_pose-6.png"
image ciel 7 = "Chars/c_0006_pose-7.png"
image ciel 8 = "Chars/c_0007_pose-8.png"
image ciel 9 = "Chars/c_0008_pose-9.png"

# title cards
image card act1_0 = im.Composite(None,
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_act1.png",
    (0,0), "Titlecards/b_deadmanwalking.png")
image card act1_1 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_0830am.png",
    (0,0), "Titlecards/b_mobiuscafe.png")
image card act1_2 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_0540pm.png",
    (0,0), "Titlecards/b_irisclass.png")
image card act1_3 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_0603pm.png",
    (0,0), "Titlecards/b_miltonhq.png")
image card act1_4 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_1000am.png",
    (0,0), "Titlecards/b_shipyard.png")
image card act1_5 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_0700pm.png",
    (0,0), "Titlecards/b_apartment.png")
image card act1_6 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_0800pm.png",
    (0,0), "Titlecards/b_vestra.png")
image card act1_7 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_1002pm.png",
    (0,0), "Titlecards/b_apartment.png")
image card act1_e = im.Composite(None,
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_act1.png",
    (0,0), "Titlecards/b_end.png")

# imagecards act 2
image card act2_0 = im.Composite(None,
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_act2.png",
    (0,0), "Titlecards/b_packhunters.png")
image card act2_1 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_0909am.png",
    (0,0), "Titlecards/b_irisclass.png")
image card act2_2 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_0956pm.png",
    (0,0), "Titlecards/b_shipyard.png")
image card act2_3 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_1116pm.png",
    (0,0), "Titlecards/b_streets.png")
image card act2_4 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_1014pm.png",
    (0,0), "Titlecards/b_unknown.png")
image card act2_5 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_1049pm.png",
    (0,0), "Titlecards/b_apartment.png")
image card act2_6 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_0915am.png",
    (0,0), "Titlecards/b_apartment.png")
image card act2_7 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_1012am.png",
    (0,0), "Titlecards/b_vestra.png")
image card act2_8 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_1134am.png",
    (0,0), "Titlecards/b_mobiuscafe.png")
image card act2_9 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_0940pm.png",
    (0,0), "Titlecards/b_unknown.png")
image card act2_e = im.Composite(None,
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_act2.png",
    (0,0), "Titlecards/b_end.png")

# titlecards act 3
image card act3_0 = im.Composite(None,
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_act3.png",
    (0,0), "Titlecards/b_monsters.png")
image card act3_1 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_1035pm.png",
    (0,0), "Titlecards/b_unknown.png")
image card act3_2 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_0819am.png",
    (0,0), "Titlecards/b_apartment.png")
image card act3_3 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_1011am.png",
    (0,0), "Titlecards/b_miltonhq.png")
image card act3_4 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_1138am.png",
    (0,0), "Titlecards/b_mobiuscafe.png")
image card act3_5 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_1007pm.png",
    (0,0), "Titlecards/b_miltonhq.png")
image card act3_6 = im.Composite(None,
    (0,0), "Titlecards/base.png",
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_1124pm.png",
    (0,0), "Titlecards/b_hideout.png")
image card act3_e = im.Composite(None,
    (0,0), "Titlecards/baseline.png",
    (0,0), "Titlecards/a_act3.png",
    (0,0), "Titlecards/b_end.png")